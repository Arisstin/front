import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Links, SearchData} from '../app.classes';
import {AppServices} from '../app.services';
import {SubscribersServices} from '../core/subscribers.services';

@Component({
  selector: 'app-breadcrumps',
  templateUrl: './breadcrumps.component.html',
  styleUrls: ['./breadcrumps.component.scss']
})
export class BreadcrumpsComponent implements OnChanges {
  @Input() searchData: SearchData;
  @Input() links: Links;
  @Input() current: string;
  params: any = {};
  constructor(public appservices: AppServices, public SS: SubscribersServices) {}
  ngOnChanges() {
    this.params = this.appservices.MSDFU(this.searchData);
  }
}
