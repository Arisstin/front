import {
  HttpRequest,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Inject } from '@angular/core';
import { tap } from 'rxjs/operators';
import {SubscribersServices} from './subscribers.services';
import { Observable } from 'rxjs';

export class JwtInterceptor implements HttpInterceptor {

  constructor(@Inject(SubscribersServices) private auth: SubscribersServices) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap(() => {}, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.auth.isAuthorized.next(false);
            localStorage.removeItem('user');
          }
        }
      }));
  }
}
