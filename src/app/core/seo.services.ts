import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';

@Injectable()
export class SeoServices extends BaseService {
  getSeoInfo(data: string): Observable<any> {
    return this.get('meta-data?seo=' + data);
  }
  getSeoUrls(): Observable<any> {
    return this.get('object-count');
  }
}
