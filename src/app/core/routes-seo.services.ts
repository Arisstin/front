import { DefaultUrlSerializer, UrlTree } from '@angular/router';

export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
  parse(url: string): UrlTree {
    const urli = url.split('?');
    urli[0] = urli[0].toLowerCase();
    const urlNew = urli.join('?');
    return super.parse(urlNew);
  }
}
