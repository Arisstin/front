import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from '../app.globals';
import {SubscribersServices} from './subscribers.services';

@Injectable()
export class BaseService {

  constructor(
    public http: HttpClient,
    public globals: Globals,
    public subscribservices: SubscribersServices
  ) {}

  get(url: string): Observable<any> {
    return this.http.get<any>(this.getUrl() + url);
  }
  post(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.getUrl() + url, data);
  }
  put(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.getUrl() + url, data);
  }
  getUrl() {
    return this.globals.BACK_END + this.subscribservices.isLanguageCurrent.value + '/';
  }
}
