import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs';

@Injectable()
export class SubscribersServices {
  isLanguageCurrent: BehaviorSubject<string> = new BehaviorSubject<string>('ru');
  isLocalSearchData: BehaviorSubject<any> = new BehaviorSubject<any>({});
  isAuthorized: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public getToken(): string {
    return localStorage.getItem('user');
  }
}
