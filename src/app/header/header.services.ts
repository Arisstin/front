import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';
import {NUser, User, ForgotInfo, SentEmail} from './header.classes';

@Injectable()
export class HeaderServices extends BaseService {
  getUserInfo(): Observable<any> {
    return this.get('users');
  }
  register(user: NUser): Observable<any> {
    return this.post('users/register', user);
  }
  login(user: User): Observable<any> {
    return this.post('users/login', user);
  }
  sentForgotPasswordEmail(value: SentEmail): Observable<any> {
    return this.post('users/recovery/email', value);
  }
  updatePassword(data: ForgotInfo): Observable<any> {
    return this.post('users/recovery', data);
  }
}
