import {Component, OnInit, TemplateRef, ViewChild, AfterViewInit, OnDestroy} from '@angular/core';
import { HeaderServices } from './header.services';
import { Globals } from '../app.globals';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import { BsModalService, BsLocaleService, BsModalRef } from 'ngx-bootstrap';
import { SubscribersServices } from '../core/subscribers.services';
import { SearchData } from '../app.classes';
import { Location } from '@angular/common';
import {AppServices} from '../app.services';
import {NUser, User, ForgotInfo, SentEmail} from './header.classes';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
  modalRef: BsModalRef;
  searchData: SearchData;
  page: string;
  modalmod: string;
  formData: any;
  currencies: string[];
  successMessage: string;
  errorMessage: string;
  urls: string[];
  loginS: any;
  @ViewChild('authmodal') authmodal: TemplateRef<any>;
  constructor(public globals: Globals,
              public SS: SubscribersServices,
              private headerservices: HeaderServices,
              private router: Router,
              private activeroute: ActivatedRoute,
              private modalService: BsModalService,
              private localserv: BsLocaleService,
              private location: Location,
              private translate: TranslateService,
              private appservice: AppServices) {
    if (localStorage.getItem('user')) {
      this.headerservices.getUserInfo().subscribe(() => { this.SS.isAuthorized.next(true); },
        () => { localStorage.removeItem('user'); this.SS.isAuthorized.next(false); });
    }
    this.currencies = Object.keys(this.globals.CURRENCY);
    this.searchData = this.SS.isLocalSearchData.value;
  }
  ngOnInit() {}
  ngAfterViewInit() {
    this.router.events.subscribe( (event) => {
      if (event instanceof NavigationEnd) {
        const url = event.url.split('?');
        this.urls = url[0].split('/');
        this.appservice.initTitle(this.urls);
        this.page = 'other';
        if (url[0] === '/' || url[0] === '/ua' || url[0] === '/en' || url[0] === '/ru') { this.page = 'main'; }
        if (url[0] === '/ru/cabinet' || url[0] === '/en/cabinet' || url[0] === '/ua/cabinet') { this.page = 'cabinet'; }
        if (url[0] === '/ua/users/recovery' || url[0] === '/en/users/recovery' || url[0] === '/ru/users/recovery') {
          console.log('recovery');
          this.formData = new ForgotInfo();
          this.formData.token = url[1].split('=')[1];
          this.modalmod = 'recovery';
          this.modalRef = this.modalService.show(this.authmodal, {class: 'login-modal'});
        }
      }
    });
  }

  changeCurrency(value: string) {
    this.searchData = this.SS.isLocalSearchData.value;
    this.searchData.currency = value;
    delete this.searchData.pr;
    this.location.go(this.router.createUrlTree([this.router.url.split('?')[0]],
      { queryParams: this.appservice.MSDFU(this.searchData) }).toString());
    this.SS.isLocalSearchData.next(this.searchData);
  }
  ChangeLanguage(value: string) {
    localStorage.setItem('language', value);
    this.translate.use(value);
    this.SS.isLanguageCurrent.next(value);
    let url = this.router.url;
    const urli = url.split('/');
    urli[1] = value;
    url = urli.join('/');
    this.location.go(this.router.createUrlTree([url.split('?')[0]],
      { queryParams: this.appservice.MSDFU(this.searchData) }).toString());
    this.localserv.use(value);
    this.appservice.initTitle(this.urls);
  }
  cleanM() {
    this.errorMessage = undefined;
    this.successMessage = undefined;
  }
  openModal(template: TemplateRef<any>, mod: string) {
    this.cleanM();
    this.modalmod = mod;
    if (mod === 'login') {
      this.formData = new User();
    } else {
      this.formData = new NUser();
    }
    this.modalRef = this.modalService.show(template, {class: 'login-modal'});
  }
  logOut() {
    localStorage.removeItem('user');
    this.SS.isAuthorized.next(false);
    if (this.page === 'cabinet') {
      this.router.navigate(['/']);
    }
  }
  login() {
    this.cleanM();
    this.headerservices.login(this.formData).subscribe(
      res => {
        localStorage.setItem('user', res.token);
        this.SS.isAuthorized.next(true);
        this.modalRef.hide();
      }, () => {
        this.errorMessage = 'login_invalid';
      }
    );
  }
  goToForgotRecovery() {
    this.cleanM();
    const email = this.formData.email;
    this.modalmod = undefined;
    this.formData = new SentEmail();
    this.formData.email = email;
    this.modalmod = 'forgot';
  }
  sentEmail() {
    this.cleanM();
    this.headerservices.sentForgotPasswordEmail(this.formData).subscribe(() => {
      this.successMessage = 'login_valid_mail';
    }, () => {
      this.errorMessage = 'login_invalid_mail';
    });
  }
  recoveryPassword() {
    this.cleanM();
    this.headerservices.updatePassword(this.formData).subscribe(() => {
      this.modalmod = undefined;
      this.formData = new User();
      this.modalmod = 'login';
    }, () => {
      this.errorMessage = 'login_invalid_token';
    });
  }
  register() {
    this.cleanM();
    this.headerservices.register(this.formData).subscribe(() => {
      this.successMessage = 'login_thx';
      }, err => {
      if (err.status === 500) {
        this.errorMessage = 'login_internet_error';
      }
      if (err.status === 409) {
        this.errorMessage = 'login_email_registried';
      }
    });
  }
}
