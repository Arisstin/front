export class User {
  email: string;
  password: string;
  constructor() {
    this.email = '';
    this.password = '';
  }
}
export class SentEmail {
  email: string;
  constructor() {
    this.email = '';
  }
}
export class NUser {
  email: string;
  password: string;
  passwordconfirm: string;
  constructor() {
    this.email = '';
    this.password = '';
    this.passwordconfirm = '';
  }
}
export class ForgotInfo {
  token: string;
  password: string;
  passwordconfirm: string;
  constructor() {
    this.token = '';
    this.password = '';
    this.passwordconfirm = '';
  }
}
