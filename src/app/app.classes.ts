export class SearchData {
  checkIn ?: any;
  checkOut ?: any;
  currency ?: string;
  seo ?: string;
  r_title ?: string;
  r_id ?: string;
  c_id ?: string;
  h_id ?: string;
  pr ?: string[];
  stars ?: number[];
  hf ?: number[];
  rf ?: number[];
  ht ?: number[];
  order_by_price ?: string;
  order_by_stars ?: string;
  offset ?: number;
  limit ?: number;
  rooms ?: any;
  lat ?: number;
  lng ?: number;
  constructor() {
    this.checkIn = new Date();
    this.checkOut = new Date();
    this.checkOut.setDate(this.checkIn.getDate() + 1);
    this.currency = 'UAH';
    this.rooms = [new Room()];
  }
}
export class Room {
  adults: number;
  children: number;
  c_ages: number[];
  constructor() {
    this.adults = 1;
    this.c_ages = [];
  }
}
export class Links {
  current: string;
  link: Link[];
  constructor(value: string) {
    this.current = value;
    this.link = [];
  }
  public init(current: string, data: Link[]) {
    this.current = current;
    this.link = data;
  }
}
export class Link {
  title: string;
  direction: any;
  id ?: string;
  constructor(direction: any, title: string, id ?: string) {
    this.direction = direction;
    this.title = title;
    if (id) { this.id = id; }
  }
}
