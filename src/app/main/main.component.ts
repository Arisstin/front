import {Component, OnDestroy} from '@angular/core';
import { SearchData } from '../app.classes';
import {AppServices} from '../app.services';
import {Router} from '@angular/router';
import {SubscribersServices} from '../core/subscribers.services';
import {SeoServices} from '../core/seo.services';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnDestroy {
  scr = [0, 0];
  minHeignt = 0;
  searchTop = 0;
  photoMobile = false;
  seoObjects: any[];
  dates = [new Date(), new Date()];
  constructor(private appservices: AppServices, private router: Router, public SS: SubscribersServices, public seo: SeoServices) {
    document.body.className = 'body-main';
    this.initSearchBlockStyle();
    const today = new Date();
    this.dates[0].setDate(today.getDate());
    this.dates[1].setDate(today.getDate() + 1);
    this.appservices.initSeo('main', '');
    this.seo.getSeoUrls().subscribe(res => {
      this.seoObjects = res;
      console.log(this.seoObjects);
    });
  }
  ngOnDestroy() {
    document.body.className = '';
    this.appservices.deleteSeo();
  }
  onSearch(data: SearchData) {
    this.router.navigate( [this.SS.isLanguageCurrent.value, 'hotels'], {queryParams: this.appservices.MSDFU(data) });
  }
  openHotel(data: any) {
    this.router.navigate( [this.SS.isLanguageCurrent.value, 'hotel', data.hotelId],
      { queryParams: this.appservices.MSDFU(data.data) });
  }
  initSearchBlockStyle() {
    this.scr = [window.innerWidth, window.innerHeight];
    if (this.scr[0] > 767) {
      this.photoMobile = false;
      this.minHeignt = this.scr[1] - 90;
      const bh = this.scr[0] < 992 ? 231 : 180;
      this.searchTop = bh > this.minHeignt ? 0 : Math.floor((this.minHeignt - bh) * 0.5);
    } else {
      this.photoMobile = this.scr[0] < this.scr[1];
      this.searchTop = 0;
      this.minHeignt = this.scr[1] - 71;
    }
  }
  checkHeight() {
    this.initSearchBlockStyle();
  }
}
