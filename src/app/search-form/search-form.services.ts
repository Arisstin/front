import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';
import {LocationList} from './locationlist/locationlist.classes';
import {SearchData} from '../app.classes';
import {SFData} from './search-form.classes';

declare let google: any;

@Injectable()
export class SearchFormServices extends BaseService {
  getNights(date1: Date, date2: Date) {
    if (date1 && date2) { return Math.ceil((+date2.setHours(0, 0, 0, 0) - +date1.setHours(0, 0, 0, 0)) / 86400000); } else { return null; }
  }
  getLocationList(value: string): Observable<LocationList> {
    return this.get('place/' + value);
  }
  modifySearch(dataold: SearchData, datanew: SFData): SearchData {
    const data = dataold;
    delete data.seo;
    delete data.hf;
    delete data.ht;
    delete data.rf;
    delete data.pr;
    delete data.stars;
    delete data.order_by_stars;
    delete data.order_by_price;
    data.offset = 0;
    data.checkIn = datanew.checkIn;
    data.checkOut = datanew.checkOut;
    data.rooms = datanew.rooms;
    if (datanew.point.h_id) {
      delete data.r_title;
      delete data.c_id;
      delete data.r_id;
      delete data.lat;
      delete data.lng;
    } else {
      data.r_title = datanew.point.r_title;
      data.lat = datanew.point.lat;
      data.lng = datanew.point.lng;
      if (datanew.point.c_id) {
        data.c_id = datanew.point.c_id;
        delete data.r_id;
      } else {
        data.r_id = datanew.point.r_id;
        delete data.c_id;
      }
    }
    return data;
  }
  modifySearchLight(dataold: SearchData, datanew: SFData): SearchData {
    const data = dataold;
    data.checkIn = datanew.checkIn;
    data.checkOut = datanew.checkOut;
    data.rooms = datanew.rooms;
    return data;
  }
}
