import {Room, SearchData} from '../app.classes';
import {ListItem} from './locationlist/locationlist.classes';
export class SFData {
  checkIn: Date;
  checkOut: Date;
  location: string;
  rooms: Room[];
  point: Point;
  constructor(data: SearchData) {
    this.checkIn = data.checkIn;
    this.checkOut = data.checkOut;
    this.rooms = data.rooms;
    this.point = new Point();
    this.location = undefined;
    if (data.r_title) {
      this.location = data.r_title;
      this.point.r_title = data.r_title;
      this.point.lat = data.lat;
      this.point.lng = data.lng;
      if (data.c_id) { this.point.c_id = data.c_id; }
      if (data.r_id) { this.point.r_id = data.r_id; }
      if (data.h_id) { this.point.h_id = data.h_id; }
    }
  }
  public init(data: ListItem) {
    this.location = data.title;
    this.point.init(data);
  }
}
export class Point {
  r_title ?: string;
  r_id ?: string;
  c_id ?: string;
  h_id ?: string;
  lat ?: number;
  lng ?: number;
  constructor() {}
  public init(data: ListItem) {
    this.r_title = data.title;
    switch (data.mod) {
      case 'cities' : {
        this.c_id = data.id;
        delete this.r_id;
        delete this.h_id;
        break;
      }
      case 'regions' : {
        this.r_id = data.id;
        delete this.c_id;
        delete this.h_id;
        break;
      }
      case 'hotels' : {
        this.h_id = data.id;
        delete this.c_id;
        delete this.r_id;
        break;
      }
    }
  }
  public seoclear() {
    delete this.r_title;
    delete this.h_id;
    delete this.r_id;
    delete this.c_id;
    delete this.lat;
    delete this.lng;
  }
  public clearcoords() {
    delete this.lat;
    delete this.lng;
  }
  public initcoords(lat: number, lng: number) {
    this.lat = lat;
    this.lng = lng;
  }
}
