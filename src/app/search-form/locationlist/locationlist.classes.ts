export class LocationList {
  hotels: ListItem[];
  regions: ListItem[];
  cities: ListItem[];
  constructor() {
    this.hotels = [];
    this.regions = [];
    this.cities = [];
  }
}
export class ListItem {
  title: string;
  id: string;
  mod ?: string;
  constructor(data: any, mod: string) {
    this.title = data.title;
    this.id = data.id;
    this.mod = mod;
  }
}
