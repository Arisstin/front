import {Component, ElementRef, EventEmitter, HostListener,
  Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import {ListItem, LocationList} from './locationlist.classes';
import {SearchFormServices} from '../search-form.services';

@Component({
  selector: 'app-locationlist',
  templateUrl: './locationlist.component.html',
  styleUrls: ['./locationlist.component.scss']
})
export class LocationlistComponent implements OnChanges {
  @Input() location: string;
  @Input() show: boolean;
  @Input() view: string;
  @Output() oncheck = new EventEmitter<ListItem>();
  locationList: LocationList;
  showview = false;
  commonlenght = 0;
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showview = false;
    }
  }
  constructor(private eRef: ElementRef, private searchformservices: SearchFormServices) {
    this.locationList = new LocationList();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (this.location && this.location.length > 2 && this.show) {
      console.log(changes);
      this.searchformservices.getLocationList(this.location).subscribe(res => {
        this.showview = true;
        this.locationList.hotels = res.hotels ? res.hotels : [];
        this.locationList.regions = res.regions ? res.regions : [];
        this.locationList.cities = res.cities ? res.cities : [];
        this.commonlenght = this.locationList.hotels.length + this.locationList.regions.length + this.locationList.cities.length;
      }, () => {
        this.commonlenght = 0;
        this.showview = false;
      });
    } else {
      this.showview = false;
    }
  }
  checkItem(checkeditem: ListItem, mod: string) {
    this.showview = false;
    const item = new ListItem(checkeditem, mod);
    this.oncheck.emit(item);
  }
}
