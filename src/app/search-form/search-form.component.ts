import {Component, EventEmitter, Input, OnInit, Output, HostListener, ElementRef } from '@angular/core';
import { SearchData } from '../app.classes';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { SubscribersServices } from '../core/subscribers.services';
import { SearchFormServices } from './search-form.services';
import { SFData, Point } from './search-form.classes';
import { Room } from '../app.classes';
import {ListItem} from './locationlist/locationlist.classes';
import {Observable} from 'rxjs';
declare var google: any;

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Input() view: string;
  @Input() dates: Date[];
  @Input() seo: boolean;
  @Output() search = new EventEmitter<SearchData>();
  @Output() openhotel = new EventEmitter<any>();
  geocorder = new google.maps.Geocoder();
  bsConfig: Partial<BsDatepickerConfig>;
  searchData: SearchData;
  searchFormData: SFData;
  showCheckNote = false;
  openCheckTravellers = false;
  dateLimits: Date[];
  childAgesRange = Array(18).fill(1);
  showlist = false;
  nights = undefined;
  viewSearch = false;
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target) && event.target.className !== 'fa fa-close') {
      this.openCheckTravellers = false;  // TODO: не происходит обновления вьюшки
    }
  }
  constructor(private subscribersservices: SubscribersServices,
              private searchformservices: SearchFormServices,
              private eRef: ElementRef) {
    this.searchFormData = new SFData(this.subscribersservices.isLocalSearchData.value);
    this.dateLimits = [new Date(), new Date()];
    this.bsConfig = { containerClass: 'theme-dark-blue', showWeekNumbers: false, dateInputFormat: 'dd, D MMMM YYYY' };
  }
  ngOnInit() {
    if (this.dates) {
      this.searchFormData.checkIn = this.dates[0];
      this.searchFormData.checkOut = this.dates[1];
      this.searchFormData.location = '';
      this.searchFormData.point = new Point();
    }
    if (this.seo) {
      this.searchFormData.point.seoclear();
      this.searchFormData.location = undefined;

    }
    if (this.searchFormData.checkIn) {
      this.dateLimits[1].setFullYear(
          this.searchFormData.checkIn.getFullYear(),
          this.searchFormData.checkIn.getMonth(),
          this.searchFormData.checkIn.getDate() + 1);
    } else {
      this.dateLimits[1] = new Date();
      this.dateLimits[1].setDate(this.dateLimits[1].getDate() + 1);
    }
  }
  cancelRoom(index: number) {
    this.searchFormData.rooms.splice(index, 1);
  }
  checkLocationItem(item: ListItem) {
    this.showlist = false;
    this.searchFormData.init(item);
    this.getLocation(item.id).subscribe(res => {
      this.searchFormData.point.initcoords(res.geometry.location.lat(), res.geometry.location.lng());
    }, () => {
      this.searchFormData.point.clearcoords();
    });
  }
  countPeople(mod: string) {
    let len = this.searchFormData.rooms.length;
    let sum = 0;
    switch (mod) {
      case 'adults': {
        while (len--) {
          sum += this.searchFormData.rooms[len].adults;
        }
        break;
      }
      case 'children': {
        while (len--) {
          sum += this.searchFormData.rooms[len].c_ages.length;
        }
        break;
      }
      case 'all': {
        while (len--) {
          sum += this.searchFormData.rooms[len].adults;
          sum += this.searchFormData.rooms[len].c_ages.length;
        }
        break;
      }
    }
    return sum;
  }
  dateChangeIn(value: Date) {
    if (value !== null) {
      const day = new Date(value.getFullYear(), value.getMonth(), value.getDate() + 1, 0, 0, 0, 0);
      this.dateLimits[1] = day;
      if (day > this.searchFormData.checkOut || !this.searchFormData.checkOut) {
        this.searchFormData.checkOut = day;
      }
      this.nights = this.searchformservices.getNights(value, this.searchFormData.checkOut);
    }
  }
  dateChangeOut(value: Date) {
    if (value !== null) {
      this.nights = this.searchformservices.getNights(this.searchFormData.checkIn, value);
    }
  }
  descrease(mod: string, roomindex ?: number) {
    switch (mod) {
      case 'adults': {
        if (this.searchFormData.rooms[roomindex].adults > 1 ||
          (this.searchFormData.rooms[roomindex].adults === 1 && this.searchFormData.rooms[roomindex].c_ages.length > 0)) {
          --this.searchFormData.rooms[roomindex].adults;
        }
        break;
      }
      case 'children': {
        if (this.searchFormData.rooms[roomindex].c_ages.length > 1 ||
          (this.searchFormData.rooms[roomindex].c_ages.length === 1 && this.searchFormData.rooms[roomindex].adults > 0)) {
          this.searchFormData.rooms[roomindex].c_ages.splice(this.searchFormData.rooms[roomindex].c_ages.length - 1, 1);
        }
        break;
      }
      case 'rooms': {
        if (this.searchFormData.rooms.length > 1) {
          this.searchFormData.rooms.splice(this.searchFormData.rooms.length - 1, 1);
        }
        break;
      }
    }
  }
  disableSearch(table ?: boolean) {
    if (this.searchFormData.location === '' && !table) { return true; }
    for (let i = 0; i < this.searchFormData.rooms.length; i++ ) {
      if (this.searchFormData.rooms[i].c_ages.indexOf(-1) > -1) {
        return true;
      }
    }
    return false;
  }
  getLocation(address: string): Observable<any> {
    return Observable.create(observer => {
      this.geocorder.geocode({
        'placeId': address
      }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          observer.next(results[0]);
          observer.complete();
        } else {
          console.log('Error: ', results, ' & Status: ', status);
          observer.error();
        }
      });
    });
  }
  increase(mod: string, roomindex ?: number) {
    switch (mod) {
      case 'adults': {
        ++this.searchFormData.rooms[roomindex].adults;
        break;
      }
      case 'children': {
        this.searchFormData.rooms[roomindex].c_ages.push(-1);
        break;
      }
      case 'rooms': {
        this.searchFormData.rooms.push(new Room());
        break;
      }
    }
  }
  searchStart() {
    this.showlist = false;
    if (!this.searchFormData.point.h_id && !this.searchFormData.point.c_id && !this.searchFormData.point.r_id && this.view !== 'table') {
      this.showCheckPoint();
    } else {
      if (this.view === 'table') {
        this.searchData = this.searchformservices.modifySearchLight(this.subscribersservices.isLocalSearchData.value, this.searchFormData);
      } else {
        this.searchData = this.searchformservices.modifySearch(this.subscribersservices.isLocalSearchData.value, this.searchFormData);
      }
      localStorage.setItem('localSearchData', JSON.stringify(this.searchData));
      if (this.searchFormData.point.h_id) {
        this.openhotel.emit({
          hotelId: this.searchFormData.point.h_id,
          data: this.searchData
        });
      } else {
        this.search.emit(this.searchData);
        this.subscribersservices.isLocalSearchData.next(this.searchData);
      }
    }
  }
  showCheckPoint() {
    this.showCheckNote = true;
    setTimeout(() => {
      this.showCheckNote = false;
    }, 3000);
  }
  trackByIdx(index: number, obj: any): any {
    return index;
  }
}
