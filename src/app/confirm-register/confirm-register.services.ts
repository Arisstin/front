import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';

@Injectable()
export class ConfirmRegisterServices extends BaseService {
  confirmToken(value: string): Observable<any> {
    return this.get('users/confirm?token=' + value);
  }
}
