import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ConfirmRegisterComponent} from './confirm-register.component';
import {ConfirmRegisterServices} from './confirm-register.services';
import {HttpLoaderFactory} from '../app.module';
const routes: Routes = [ { path: '', component: ConfirmRegisterComponent} ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    ConfirmRegisterComponent
  ],
  providers: [
    ConfirmRegisterServices
  ]
})
export class ConfirmRegisterModule { }
