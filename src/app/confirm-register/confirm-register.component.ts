import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ConfirmRegisterServices} from './confirm-register.services';
import {SubscribersServices} from '../core/subscribers.services';
@Component({
  selector: 'app-confirm-register',
  templateUrl: './confirm-register.component.html',
  styleUrls: ['./confirm-register.component.scss']
})
export class ConfirmRegisterComponent {
  mod = 0;
  token: string;
  constructor(private router: Router,
              private activerouter: ActivatedRoute,
              private confregservices: ConfirmRegisterServices,
              private SS: SubscribersServices) {
    this.token = this.activerouter.snapshot.paramMap.get('token');
    if (this.token) {
      this.confregservices.confirmToken(this.token).subscribe(resa => {
        this.mod = 3;
        this.SS.isAuthorized.next(true);
        localStorage.setItem('user', resa.token);
      }, err => {
        if (err.status === 409 || err.status === 404) {
          this.mod = 1;
        }
        if (err.status === 500) { this.mod = 2; }
      });
    } else {
      this.router.navigate(['/']);
    }
  }
  goToMain() {
    this.router.navigate(['/']);
  }
  gotoCabinet() {
    this.router.navigate(['/' + this.SS.isLanguageCurrent.value, 'cabinet']);
  }

}
