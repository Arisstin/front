export class Globals {
  FRONT_END = 'https://bronirovka.ua';
  // BACK_END = 'https://api.bronirovka.ua/';
  IMAGE_URL = 'https://images.bronirovka.ua/';
  BACK_END = 'http://localhost:8000/';
  // FRONT_END = 'http://192.168.0.101:4200';
  // BACK_END = 'http://192.168.0.106:8000/';
  // IMAGE_URL = 'http://192.168.0.100/';
  HOTEL_LIMIT = 20;
  JOIN_HREF = 'http://join.bronirovka.ua';
  CURRENCY = {uah: 'UAH', usd: 'USD', eur: 'EUR'};
  // 'http://192.168.0.101:8000/';         'http://api.bronirovka.ua/';
}

