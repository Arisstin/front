import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscribersServices} from '../core/subscribers.services';
import {AppServices} from '../app.services';
import {HotelServices} from './hotel.services';
import {Links, SearchData, Link} from '../app.classes';
import {Globals} from '../app.globals';
import {CarouselImagesInfo, Hotel, HotelView} from './hotel.classes';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit, OnDestroy {
  @ViewChild('carousel') carousel: ElementRef;
  hotelInfo: Hotel;
  hotelViewList: string[];
  hotelview: HotelView[] = [];
  hotelAdd: any[];
  subscribers = [];
  firstSDS = true;
  CIs: CarouselImagesInfo;
  searchData: SearchData;
  hotelId: string;
  routeparams: any;
  loading = true;
  modalRef: BsModalRef;
  links: Links;
  constructor(private activeroute: ActivatedRoute,
              private router: Router,
              public SS: SubscribersServices,
              private appservices: AppServices,
              private hotelservices: HotelServices,
              private globals: Globals,
              private modalservice: BsModalService,
              private title: Title) {
    document.body.className = 'body-hotel';
    this.links = new Links('breadcrumps_hotel');
    this.hotelAdd = [];
    this.searchData = this.SS.isLocalSearchData.value;
    const country = this.activeroute.snapshot.paramMap.get('country');
    const city = this.activeroute.snapshot.paramMap.get('city');
    if (country) {
      this.links.link.push(new Link(country, country));
      if (city) {
        this.links.link.push(new Link(country + '/' + city, city));
      }
    } else {
      this.links.link.push(new Link('', this.appservices.getDirectionBreadcrump(this.searchData)[0]));
    }
    this.hotelId = this.activeroute.snapshot.paramMap.get('id');
    this.appservices.initSeo(this.hotelId, '/' + this.SS.isLanguageCurrent.value + '/hotel/' + this.hotelId);
    this.hotelViewList = JSON.parse(localStorage.getItem('hotelViewed'));
    if (!this.hotelViewList) { this.hotelViewList = []; }
    this.loadview();
  }
  ngOnInit() {
    this.subscribers.push(this.SS.isLanguageCurrent.subscribe(() => {
      if (this.firstSDS) {
        this.firstSDS = false;
        this.loading = true;
      }
      this.load();
    }));
    this.subscribers.push(this.SS.isLocalSearchData.subscribe(res => {
      if ((this.searchData.currency !== res.currency) && this.hotelInfo.stars) {
        this.loadrecomended(this.hotelInfo.stars.length);
      }
      this.searchData = res;
      this.routeparams = this.appservices.MSDFU(this.searchData);
    }));
  }
  closeView(id: string, index: number) {
    const ind = this.hotelViewList.indexOf(id);
    this.hotelViewList.splice(ind, 1);
    localStorage.setItem('hotelViewed', JSON.stringify(this.hotelViewList));
    this.hotelview.splice(index, 1);
  }
  loadrecomended(stars: number) {
    this.hotelservices.getrecomended(this.hotelId, stars, this.appservices.CSDTS(this.searchData)).subscribe(res => {
      this.hotelAdd = res.result ? res.result : [];
      this.hotelAdd.forEach((_, index) => {
        this.hotelAdd[index].stars = this.hotelAdd[index].stars ? Array(this.hotelAdd[index].stars).fill(1) : [];
        this.hotelAdd[index].currency = this.searchData.currency;
        if (!this.hotelAdd[index].photo) { this.hotelAdd[index].photo = '../../assets/images/hotel-thumb-default.jpg'; } else {
          if (this.hotelAdd[index].bb) {
            this.hotelAdd[index].photo = 'http://photos.hotelbeds.com/giata/' + this.hotelAdd[index].photo;
          } else {
            this.hotelAdd[index].photo = this.globals.IMAGE_URL + this.hotelAdd[index].hotelid + '/' + this.hotelAdd[index].photo;
          }
        }
      });
    });
  }
  load() {
    this.hotelservices.getHotelInfo(this.hotelId).subscribe(res => {
      if (!this.hotelInfo || this.hotelInfo.id !== res.id) {
        this.CIs = new CarouselImagesInfo(res.photos.length);
      }
      this.resizeCarousel();
      this.CIs.initClasses();
      this.links.current = res.title;
      this.title.setTitle(res.title);
      this.loadrecomended(res.stars);
      this.hotelInfo = new Hotel(res, this.globals.IMAGE_URL);
      if (!this.hotelViewList.includes(this.hotelInfo.id)) {
        this.hotelViewList.push(this.hotelInfo.id);
        localStorage.setItem('hotelViewed', JSON.stringify(this.hotelViewList));
      }
      this.loading = false;
    }, () => {
      this.links.current = 'breadcrumps_not_found';
      this.hotelInfo = undefined;
      this.loading = false;
    });
  }
  loadview() {
    let lpindex = 0;
    this.hotelViewList.reverse().forEach((item, index) => {
      if (lpindex++ < 7) {
        this.hotelservices.getHotelInfo(item).subscribe(res => {
          this.hotelview.push(new HotelView(res));
          if (this.hotelview[this.hotelview.length - 1].photo) {
            this.hotelview[this.hotelview.length - 1].photo =
              res.bb ? 'http://photos.hotelbeds.com/giata/' + this.hotelview[this.hotelview.length - 1].photo :
                this.globals.IMAGE_URL + res.id + '/' + this.hotelview[this.hotelview.length - 1].photo;
          } else {
            this.hotelview[this.hotelview.length - 1].photo = '../../assets/images/hotel-thumb-default.jpg';
          }
          },
          () => {
          this.hotelViewList.splice(index, 1);
          localStorage.setItem('hotelViewed', JSON.stringify(this.hotelViewList));
        });
      }
    });
  }
  ngOnDestroy() {
    document.body.className = '';
    this.subscribers[0].unsubscribe();
    this.subscribers[1].unsubscribe();
    this.appservices.deleteSeo();
  }
  onSearch(event) {
    this.router.navigate( ['/' + this.SS.isLanguageCurrent.value, 'hotels'], {queryParams: this.appservices.MSDFU(event) });
  }
  openMap(template: TemplateRef<any>) {
    this.modalRef = this.modalservice.show(template, {class: 'modal-map'});
  }
  hotelOpen(data: any) {
    this.hotelId = data.hotelId;
    if (data.data) { this.searchData = data.data; }
    this.loading = true;
    this.load();
  }
  resizeCarousel(img ?: any) {
    if (img && this.carousel.nativeElement.offsetWidth < 60) {
      this.CIs.resize(img.offsetWidth);
    } else {
      this.CIs.resize(this.carousel.nativeElement.offsetWidth - 30);
    }
  }
  mouseClickCar(value: boolean) {
    this.CIs.carouselmove = value;
    // if (!value) {
    //   this.CIs.centring();
    // }
  }
  mouseMove(event) {
    this.CIs.move(event);
  }
  centerImageCar(index: number) {
    this.CIs.center(index);
  }
}
