import {findIndex} from "rxjs/operators";

export class HotelView {
  photo ?: string;
  id: string;
  title: string;
  stars: number[];
  constructor(data: any) {
    this.title = data.title;
    this.stars = data.stars ? Array(data.stars).fill(1) : [];
    if (data.photos.length > 0) {
     let avatarPhoto = data.photos.find(function (photo) {
        return photo.isAvatar === true;
      });
      if (!avatarPhoto) {
        avatarPhoto = data.photos[0];
      }
      this.photo = data.bb ? avatarPhoto.path : avatarPhoto.name;
    }
    if (data.bb) {
      this.id = data.id + '_bb';
    } else {
      this.id = data.id;
    }
  }
}
export class Hotel {
  id: string;
  bb: boolean;
  title: string;
  photos: string[];
  photosmin: string[];
  description: string[];
  location: Location;
  stars: number[];
  payment ?: any;
  facility ?: any;
  facilities ?: any[];
  registry ?: Facility;
  policy ?: Policy;
  constructor(data: any, imgurl: string) {
    let loopindex = 0;
    this.id = data.id;
    if (data.bb) {
      this.bb = true;
      this.id += '_bb';
    }
    this.photos = [];
    this.photosmin = [];
    this.description = [];
    this.payment = data.payment ? data.payment : {cards: []};
    this.registry = data.registry ? data.registry : undefined;
    this.policy = new Policy(data.policy);
    this.title = data.title;
    this.stars = data.stars ? Array(data.stars) : [];
    this.location = new Location(data.location);
    if (data.photos) {
      loopindex = data.photos.length;
      if (data.bb) {
        while (loopindex--) {
          this.photos.push('http://photos.hotelbeds.com/giata/bigger/' + data.photos[loopindex].path);
          this.photosmin.push('http://photos.hotelbeds.com/giata/small/' + data.photos[loopindex].path);
        }
      } else {
        while (loopindex--) {
          this.photos.push(imgurl + data.id + '/' + data.photos[loopindex].name);
          this.photosmin.push(imgurl + data.id + '/' + data.photos[loopindex].name);
        }
      }
    }
    if (data.description) {
      if (data.bb) {
        const desc = data.description.split('.');
        this.description.push([desc[0], desc[1]].join('.'));
        this.description[0] += '.';
        desc.splice(0, 2);
        if (desc.length > 0) { this.description.push(desc.join('.')); }
      } else {
        this.description.push(data.description.location);
        this.description.push(data.description.rooms);
        this.description.push(data.description.nutrition);
        this.description.push(data.description.environment);
      }
    }
    if (data.facility || data.facilities) {
      this.facility = this.bb ? new Facility() : new Facility(data.facility);
      const facilList = this.bb ? data.facilities : data.facility.facilities;
      facilList.forEach(item => {
        switch (item.type) {
          case 'INTERNET' : {
            this.facility.internet.push([item.title]);
            break;
          }
          case 'ARRIVAL' : {
            this.policy.arrival = item.timeFrom;
            this.policy.departure = item.timeTo ? item.timeTo : '12:00';
            break;
          }
          case 'CARD' : {
            this.payment.cards.push(item.title);
            break;
          }
          case 'ANIMALS' : {
            this.policy.animals.push([item.title]);
            break;
          }
          case 'CHILDREN' : {
            // this.policy.children.push([item.title]);
            this.policy.children.push(['hotelpage_childbed_hb']);
            break;
          }
          default: {
            if (item.belongTo === 'HOTEL') {
              this.facility.facilities.push(item);
            }
            if (!item.type) {
              this.facility.facilities.push(item);
              this.facility.facilities[this.facility.facilities.length - 1].type = 'ANOTHER';
            }
            break;
          }
        }
      });
      loopindex = this.facility.facilities.length;
      while (loopindex > 1 && loopindex--) {
        for (let i = 0; i < loopindex; i++) {
          if (this.facility.facilities[loopindex].title === this.facility.facilities[i].title) {
            this.facility.facilities.splice(loopindex, 1);
            break;
          }
        }
      }
      this.facility.formTypes();
    }
  }
}
export class Facility {
  facilities: any[];
  facilityTypes: string[];
  internet ?: any[];
  reception ?: any;
  parking ?: any[];
  constructor(data ?: any) {
    this.facilityTypes = [];
    this.facilities = [];
    this.internet = [];
    if (data) {
      this.internet.push([]);
      this.reception = data.reception ? data.reception : undefined;
      this.parking = data.parking ? data.parking : undefined;
      if (data.internet.access === 'NO') {
        this.internet[0].push('hotelpage_no');
      } else {
        if (data.internet.access === 'FREE') { this.internet[0].push('hotelpage_int_free');
        } else { this.internet[0].push('hotelpage_int_pay'); }
        if (data.internet.type === 'WI-FI') { this.internet[0].push('hotelpage_int_wi_fi');
        } else { this.internet[0].push('hotelpage_int_cabel'); }
        switch (data.internet.accessPoint) {
          case 'ALL_ROOMS': { this.internet[0].push('hotelpage_int_everyrooms'); break; }
          case 'SOME_ROOMS': { this.internet[0].push('hotelpage_int_someroom'); break; }
          case 'PUBLIC_AREAS': { this.internet[0].push('hotelpage_int_public'); break; }
          case 'EVERYWHERE': { this.internet[0].push('hotelpage_int_everywhere'); break; }
        }
        if (data.internet.price > 0) { this.internet[0].push('hotelpage_for'); this.internet.push(data.internet.price); }
      }
    }
  }
  public formTypes() {
    let i = this.facilities.length;
    while (i--) {
        if (this.facilityTypes.indexOf(this.facilities[i].type) < 0 && this.facilities[i].belongTo === 'HOTEL' && this.facilities[i].type) {
          this.facilityTypes.push(this.facilities[i].type);
        }
    }
  }
}
export class Policy {
  arrival: string;
  departure: string;
  isAdultOnly ?: boolean;
  children: any[];
  animals: any[];
  constructor(data: any) {
    this.children = [];
    this.animals = [];
    if (data) {
      this.arrival = data.arrival;
      this.departure = data.departure;
      this.isAdultOnly = data.isAdultOnly;
      this.animals.push([]);
      if (data.animails === 'FORBIDDEN') { this.animals[0].push('hotelpage_animals_deny');
      } else { if (data.animails === 'ALLOWED') { this.animals[0].push('hotelpage_animals_free');
      } else { this.animals[0].push('hotelpage_animals_admin'); } }
      if (data.children) {
        data.children.forEach((child, i) => {
          this.children.push([]);
          this.children[i].push('hotelpage_children_from', child.ageFrom, 'hotelpage_children_to', child.ageTo, 'hotelpage_will_place');
          if (child.bad === 'AVAILABLE_BEDS') { this.children[i].push('hotelpage_available_beds'); }
          if (child.bad === 'EXTRA_BEDS') { this.children[i].push('hotelpage_add_beds'); }
          if (child.bad === 'CHILDRENS_BEDS') { this.children[i].push('hotelpage_child_beds'); }
          if (child.price > 0) { this.children[i].push('hotelpage_for', child.price); } else { this.children[i].push('hotelpage_free'); }
        });
      }
    }
  }
}
export class Location {
  address: string;
  lat: number;
  lng: number;
  constructor(data) {
    this.address = data.address;
    this.lat = data.lat;
    this.lng = data.lng;
  }
}
export class CarouselImagesInfo {
  margin: number[];
  distances: number[];
  zindex: string[];
  carouselmove: boolean;
  widthblock: number;
  left: number;
  activeSlide: number;
  maxLength: number;
  constructor(lenght: number) {
    this.activeSlide = 0;
    this.maxLength = 0;
    this.margin = Array(lenght).fill(0);
    this.distances = Array(lenght).fill(0);
    this.zindex = Array(lenght).fill('hide');
    this.left = 0;
    this.widthblock = 0;
    this.carouselmove = false;
  }
  public resize(width: number) {
    this.widthblock = width;
    if (window.innerWidth > 767) {
      this.margin.fill(width / 4);
      this.margin[0] = width / 2;
    } else {
      if (window.innerWidth > 575) {
        this.margin.fill(width / 2);
        this.margin[0] = width / 2;
      } else {
        this.margin.fill(width);
        this.margin[0] = width / 2;
      }
    }
    if (this.distances.length > 1) {
      this.distances = this.distances.map((x, k) => {
        return this.margin[0] + k * this.margin[1];
      });
    }
    if (this.distances.length > 0) {
      this.distances[0] = width / 2;
    }
    this.maxLength = this.margin.length < 2 ? 0 : this.margin[1] * (this.margin.length - 1);
  }
  public initClasses() {
    const di = this.distances.map(x => {
      return Math.abs(x - Math.abs(this.left) - this.widthblock / 2);
    });
    const indexMin = di.indexOf(Math.min(...di));
    if (indexMin > -1) {
      this.zindex.fill('hide');
      this.zindex[indexMin] = 'first';
      this.activeSlide = indexMin;
      if (this.zindex[indexMin - 1]) { this.zindex[indexMin - 1] = 'second'; }
      if (this.zindex[indexMin + 1]) { this.zindex[indexMin + 1] = 'second'; }
      if (this.zindex[indexMin - 2]) { this.zindex[indexMin - 2] = 'third'; }
      if (this.zindex[indexMin + 2]) { this.zindex[indexMin + 2] = 'third'; }
    }
  }
  public move(moveX) {
    if (this.carouselmove) {
      if (this.left + moveX <= 0 && this.left + moveX >= -(this.maxLength)) {
        this.left = this.left + moveX;
      }
      this.initClasses();
    }
  }
  public center(index: number) {
    this.activeSlide = index;
    this.left = -(index * this.margin[1]);
    this.initClasses();
  }
}
