import { Injectable } from '@angular/core';
import {BaseService} from '../core/base.services';
import {Observable} from 'rxjs';

@Injectable()
export class HotelServices extends BaseService {
  getHotelRooms(id: string, url: URLSearchParams): Observable<any> {
    return this.get('hotel/' + id + '/prices-for-period?' + url);
  }
  getHotelInfo(id: string): Observable<any> {
    return this.get('hotel/' + id);
  }
  getrecomended(id: string, stars: number, url: URLSearchParams): Observable<any> {
    return this.get('recommend-hotels?' + url + '&id_exclude=' + id + '&stars[]=' + stars);
  }
}
