import {Component, OnDestroy, OnInit, Input, ViewChild, ElementRef, TemplateRef} from '@angular/core';
import {Location} from '@angular/common';
import {trigger, style, transition, animate, state} from '@angular/animations';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Globals} from '../../app.globals';
import {SearchData} from '../../app.classes';
import {HotelServices} from '../hotel.services';
import {Router} from '@angular/router';
import {SubscribersServices} from '../../core/subscribers.services';
import {AppServices} from '../../app.services';

@Component({
  selector: 'app-hotel-table',
  templateUrl: './hoteltable.component.html',
  styleUrls: ['./hoteltable.component.scss'],
  animations: [
    trigger('extended', [
      state('show', style({
        height: '*',
        opacity: '1',
        overflow: 'visible' })),
      state('hide', style({
        height: '0',
        opacity: '0',
        overflow: 'hidden' })),
      state('open', style({ transform: 'rotate(90deg)' })),
      state('close', style({ })),
      transition('hide <=> show', animate('300ms linear')),
      transition('open <=> close', animate('300ms linear'))
    ])
  ]
})
export class HoteltableComponent implements OnInit, OnDestroy {
  @Input() hotelId: string;
  @Input() payments: any;
  @ViewChild('table') tableTariff: ElementRef;
  @ViewChild('tablehead') tableHead: ElementRef;
  bb = false;
  RA: any[];
  SD: SearchData;
  loading = true;
  lix = 0;
  roomCheckIndex = 0;
  roomCheckSlide = 0;
  photoModal: BsModalRef;
  totalPrice = 0;
  subscribers: any[] = [];
  firstlansub = true;
  HBNutrition: any;
  HNutrition: any;
  displaySecondHead = false;
  routeparams = [];
  constructor(private hotelservices: HotelServices,
              private router: Router,
              private location: Location,
              private modalservice: BsModalService,
              private globals: Globals,
              private SS: SubscribersServices,
              private appservices: AppServices) {
    this.routeparams = this.router.url.split('?')[0].split('/');
    this.routeparams.splice(0, 1);
    this.SD = this.SS.isLocalSearchData.value;
    this.HBNutrition = {
      AB: 'hotelpagetable_AB',
      AI: 'hotelpagetable_AI',
      AS: 'hotelpagetable_AS',
      BB: 'hotelpagetable_BB',
      BH: 'hotelpagetable_BH',
      CB: 'hotelpagetable_CB',
      BF: 'hotelpagetable_BF',
      DD: 'hotelpagetable_DD',
      DX: 'hotelpagetable_DX',
      FB: 'hotelpagetable_FB',
      FH: 'hotelpagetable_FH',
      FL: 'hotelpagetable_FL',
      FR: 'hotelpagetable_FR',
      FS: 'hotelpagetable_FS',
      EB: 'hotelpagetable_EB',
      HB: 'hotelpagetable_HB',
      HL: 'hotelpagetable_HL',
      HR: 'hotelpagetable_HR',
      HS: 'hotelpagetable_HS',
      IB: 'hotelpagetable_IB',
      LB: 'hotelpagetable_LB',
      HV: 'hotelpagetable_HV',
      FV: 'hotelpagetable_FV',
      QS: 'hotelpagetable_QS',
      RO: 'hotelpagetable_RO',
      SB: 'hotelpagetable_SB',
      SC: 'hotelpagetable_SC'
    };
    this.HNutrition = {
      BREAKFAST: 'hotelpagetable_breakfast',
      HALF_BOARD: 'hotelpagetable_halfboard',
      FULL_BOARD: 'hotelpagetable_fullboard',
      BREAKFAST_INCLUDED: 'hotelpagetable_breakfast_included',
      HALF_BOARD_INCLUDED: 'hotelpagetable_halfboard_included',
      FULL_BOARD_INCLUDED: 'hotelpagetable_fullboard_included',
      NO_NUTRITION: 'hotelpagetable_nonutrition'
    };
  }
  ngOnInit() {
    this.bb = this.hotelId.split('_')[1] === 'bb';
    this.subscribers.push(this.SS.isLocalSearchData.subscribe(res => {
      this.SD = res;
      console.log(this.SD);
      if (this.SD.checkIn && this.SD.checkOut) {
        this.load();
      }
    }));
    this.subscribers.push(this.SS.isLanguageCurrent.subscribe(() => {
      if (this.firstlansub) {
        this.firstlansub = false;
      } else {
        this.load();
      }
    }));
  }
  ngOnDestroy() {
    this.lix = this.subscribers.length;
    while (this.lix--) { this.subscribers[this.lix].unsubscribe(); }
  }
  bookingChecked() {
    let count = 0;
    this.RA.forEach( item => { item.tariffs.forEach(it => { count += it._checked_count; }); });
    return count <= 0;
  }
  bedImage(type: string) {
    switch (type) {
      case 'BUNK' : { return 'bunk.svg'; }
      case 'ARMBED' : { return 'chair.svg'; }
      default: {
        if (type[0] === 'S') {
          return type.includes('SOFA') ? 'sofa-1.svg' : 'bed-1.svg';
        } else {
          return type.includes('SOFA') ? 'sofa-2.svg' : 'bed-2.svg';
        }
      }
    }
  }
  openPhotoModal(template: TemplateRef<any>, index: number) {
    if (this.RA[index].photo.length > 0 || this.RA[index].facilities.length > 0) {
      this.roomCheckIndex = index;
      this.photoModal = this.modalservice.show(template, {class: 'photo-modal'});
    }
  }
  countTotalPrice() {
    this.totalPrice = 0;
    this.RA.forEach(item => {
      item.tariffs.forEach(it => {
        this.totalPrice += it._checked_count * it.priceWithDiscount;
      });
    });
  }
  checkRoomsCount(ri: number, ti: number, value: number) {
    let sum = 0;
    this.RA[ri].tariffs.forEach((tariff, index) => {
      if (index !== ti) {
        sum += Number(this.RA[ri].tariffs[index]._checked_count);
      }
    });
    this.RA[ri].tariffs.forEach((tariff, index) => {
      if (index !== ti) {
        this.RA[ri].tariffs[index]._count = this.RA[ri].count - Number(value) - sum +
          this.RA[ri].tariffs[index]._checked_count;
      } else {
        this.RA[ri].tariffs[index]._checked_count = Number(value);
      }
    });
    console.log(this.RA);
    this.countTotalPrice();
  }
  load() {
    if (!this.RA) { this.loading = true; }
    this.totalPrice = 0;
    this.hotelservices.getHotelRooms(this.hotelId, this.appservices.CSDTS(this.SD)).subscribe(res => {
      if (res) {
        this.RA = res;
        this.RA.forEach((room, ind) => {
          this.lix = room.tariffs.length;
          while (this.lix-- && this.lix > 1) {
            let li = this.lix;
            while (li--) {
              if (room.tariffs[this.lix].price === room.tariffs[li].price &&
                room.tariffs[this.lix].nutrition === room.tariffs[li].nutrition) {
                this.RA[ind].tariffs.splice(this.lix, 1);
                break;
              }
            }
          }
        });
        this.RA.forEach((room, index) => {
          this.RA[index]['showfacil'] = false;
          this.RA[index]['bedTransform'] = 'S';
          this.RA[index]['photo'] = [];
          this.RA[index]['photomin'] = [];
          if (!this.RA[index].photos) { this.RA[index].photos = []; }
          if (this.bb) {
            this.RA[index].count = Number(this.RA[index].tariffs[0].count);
            this.RA[index]._bookcount = new Array(Number(this.RA[index].tariffs[0].count) + 1).fill(1);
          } else {
            this.RA[index]._bookcount = Array(room.count + 1).fill(1);
          }
          this.lix = this.RA[index].photos.length;
          if (!this.bb) {
            this.RA[index].guests = Array(room.guest).fill(1);
            while (this.lix--) {
              this.RA[index].photo.push(this.globals.IMAGE_URL + this.hotelId + '/' + this.RA[index].photos[this.lix].name);
              this.RA[index].photomin.push(this.globals.IMAGE_URL + this.hotelId + '/' + this.RA[index].photos[this.lix].name);
            }
          } else {
            this.RA[index].childrenarr = Array(room.children).fill(1);
            while (this.lix--) {
              this.RA[index].photo.push('http://photos.hotelbeds.com/giata/bigger/' + this.RA[index].photos[this.lix].path);
              this.RA[index].photomin.push('http://photos.hotelbeds.com/giata/small/' + this.RA[index].photos[this.lix].path);
            }
          }
          room.tariffs.forEach( (tariff, indextar) => {
            this.RA[index].tariffs[indextar]._checked_count = 0;
            this.RA[index].tariffs[indextar]._count = room.count;
            if (!this.bb) {
              tariff.nutritions.forEach((nut, indexnut) => {
                if (nut.type === 'HALF_BOARD' || nut.type === 'HALF_BOARD_INCLUDED') {
                  this.RA[index].tariffs[indextar].nutritions[indexnut]['nutririontooltip'] = 'hotelpagetable_halfboard_description';
                }
                if (nut.type === 'FULL_BOARD' || nut.type === 'FULL_BOARD_INCLUDED') {
                  this.RA[index].tariffs[indextar].nutritions[indexnut]['nutririontooltip'] = 'hotelpagetable_fullboard_description';
                }
                if (nut.type === 'NO_NUTRITION') {
                  this.RA[index].tariffs[indextar].nutritions[indexnut]['nutririontooltip'] = 'hotelpagetable_nonuntrition_description';
                }
              });
            } else {
              if (!this.RA[index].tariffs[indextar].priceWithDiscount) {
                this.RA[index].tariffs[indextar].priceWithDiscount = this.RA[index].tariffs[indextar].price;
              }
              if (this.RA[index].tariffs[indextar].nutrition === 'RO') {
                this.RA[index].tariffs[indextar]['nutririontooltip'] = 'hotelpagetable_nonuntrition_description';
              }
              if (this.RA[index].tariffs[indextar].nutrition === 'FH' ||
                this.RA[index].tariffs[indextar].nutrition === 'FB' ||
                this.RA[index].tariffs[indextar].nutrition === 'FV') {
                this.RA[index].tariffs[indextar]['nutririontooltip'] = 'hotelpagetable_fullboard_description';
              }
              if (this.RA[index].tariffs[indextar].nutrition === 'BH' ||
                this.RA[index].tariffs[indextar].nutrition === 'HB' ||
                this.RA[index].tariffs[indextar].nutrition === 'HV') {
                this.RA[index].tariffs[indextar]['nutririontooltip'] = 'hotelpagetable_halfboard_description';
              }
              this.RA[index].tariffs[indextar].guests = Array(tariff.guest).fill(1);
              this.RA[index].tariffs[indextar].childrens = Array(tariff.children).fill(1);
            }
          });
          if (this.bb && room.facilities) {
            const areaind = room.facilities.findIndex(x => x.code === 295);
            if (areaind > -1) {
              this.RA[index].area = room.facilities[areaind].number;
              this.RA[index].facilities.splice(areaind, 1);
            }
          }
        });
      } else {
        this.RA = undefined;
      }
      this.loading = false;
    }, err => {
      this.loading = false;
      this.RA = undefined;
    });
  }
  hasTransformBed(beds: any[]) { return beds.find(x => x.type.isTransformed === true); }
  search(event: SearchData) {
    this.location.go(this.router.createUrlTree([this.SS.isLanguageCurrent.value, 'hotel', this.hotelId],
      { queryParams: this.appservices.MSDFU(event) }).toString());
  }
  startBooking() {
    let bookinfo: any = {};
    if (this.bb) {
      const tariffArray = [];
      this.RA.forEach( room => {
        room.tariffs.forEach( tariff => {
          if (tariff._checked_count !== 0) {
            tariffArray.push(tariff);
            tariffArray[tariffArray.length - 1]['roomtitle'] = room.title;
            tariffArray[tariffArray.length - 1]['guest'] = room.guest;
          }
        });
      });
      bookinfo = { hotel: this.hotelId, tariffs: tariffArray, rooms: this.SD.rooms };
      localStorage.setItem('bookingInfo', JSON.stringify(bookinfo));
      this.routeparams.push('book-hb');
      this.router.navigate(this.routeparams);
    } else {
      let counttariffs = false;
      bookinfo['hotel'] = this.hotelId;
      bookinfo['beds'] = [];
      bookinfo['tariffs'] = [];
      this.RA.forEach( room => {
        room.tariffs.forEach( tariff => {
          if (tariff._checked_count !== 0) {
            bookinfo.tariffs.push({id: tariff.id, count: tariff._checked_count});
            bookinfo.beds.push(room.bedTransform);
            counttariffs = true;
          }
        });
      });
      localStorage.setItem('bookingInfo', JSON.stringify(bookinfo));
      this.routeparams.push('book');
      this.router.navigate(this.routeparams);
    }
  }
  stateCaret(value: boolean) { return value ? 'open' : 'close'; }
  stateName(value: boolean) { return value ? 'show' : 'hide'; }
  tableScroll() {
    if (this.tableTariff.nativeElement.offsetHeight > 450 && this.tableTariff.nativeElement.getBoundingClientRect().top < 0 &&
      this.tableTariff.nativeElement.offsetHeight + this.tableTariff.nativeElement.getBoundingClientRect().top > 220) {
      this.displaySecondHead = true;
    } else {
      this.displaySecondHead = false;
    }
  }
  trackByIdx(index: number, obj: any): any {
    return index;
  }
}
