import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory} from '../app.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {RouterModule, Routes} from '@angular/router';
import {MylistComponent} from './mylist.component';

const routes: Routes = [ { path: '', component: MylistComponent} ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    MylistComponent
  ],
  providers: []
})
export class MylistModule { }
