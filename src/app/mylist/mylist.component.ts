import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppServices} from '../app.services';
import {SubscribersServices} from '../core/subscribers.services';
import {HotelServices} from '../hotel/hotel.services';
import {HotelView} from '../hotel/hotel.classes';
import {Globals} from '../app.globals';

@Component({
  selector: 'app-mylist',
  templateUrl: './mylist.component.html',
  styleUrls: ['./mylist.component.scss']
})
export class MylistComponent {
  hotels: any[];
  hotelList: string[];
  params: any;
  loading = true;
  translatesub: any;
  constructor(appservices: AppServices, public SS: SubscribersServices, hotelservices: HotelServices, globals: Globals) {
    this.params = appservices.MSDFU(this.SS.isLocalSearchData.value);
    this.hotelList = JSON.parse(localStorage.getItem('hotelViewed'));
    if (!this.hotelList || this.hotelList.length === 0) { this.loading = false; }
    this.hotels = [];
    if (!this.hotelList) { this.hotelList = []; }
    this.hotelList.reverse().forEach((item, index) => {
        hotelservices.getHotelInfo(item).subscribe(res => {
            this.hotels.push(new HotelView(res));
            if (this.hotels[this.hotels.length - 1].photo) {
              this.hotels[this.hotels.length - 1].photo =
                res.bb ? 'http://photos.hotelbeds.com/giata/' + this.hotels[this.hotels.length - 1].photo :
                  globals + res.id + '/' + this.hotels[this.hotels.length - 1].photo;
            } else {
              this.hotels[this.hotels.length - 1].photo = '../../assets/images/hotel-thumb-default.jpg';
            }
            this.loading = false;
          },
          () => {
          this.loading = false;
            this.hotelList.splice(index, 1);
            localStorage.setItem('hotelViewed', JSON.stringify(this.hotelList));
          });
    });
  }
  closeView(id: string, index: number) {
    const ind = this.hotelList.indexOf(id);
    this.hotelList.splice(ind, 1);
    localStorage.setItem('hotelViewed', JSON.stringify(this.hotelList));
    this.hotels.splice(index, 1);
  }
}
