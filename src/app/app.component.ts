import {Component} from '@angular/core';
import {SubscribersServices} from './core/subscribers.services';
import {TranslateService} from '@ngx-translate/core';
import {AppServices} from './app.services';
import {BsLocaleService} from 'ngx-bootstrap';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  language: string;
  constructor(subscribers: SubscribersServices,
              translate: TranslateService,
              localserv: BsLocaleService,
              appservice: AppServices,
              router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
    this.language = appservice.initLanguage(translate.getBrowserLang());
    translate.use(this.language);
    localserv.use(this.language);
    appservice.listenUrlParams();
  }
}
