import {Component, OnDestroy, OnInit} from '@angular/core';
import * as moment from 'moment';
import {Globals} from '../app.globals';
import {UserServices} from './user.services';
import {Router} from '@angular/router';
import {UserInfo, Initials} from './user.classes';
import {AppServices} from '../app.services';
import {SubscribersServices} from '../core/subscribers.services';
import {BsDatepickerConfig} from 'ngx-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  activeMenuItem: number;
  bookings: any[];
  loaded = false;
  userInfo: UserInfo;
  initials: Initials;
  countriesList: any[] = [];
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  bookInfo: any;
  bookInfoLoaded = false;
  bookInfoEdited = false;
  minDatesEdit = [new Date(), new Date()];
  params: any;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  mask = ['+', /\d/, /\d/, ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  currency: any;
  imageUrl: string;
  translateSubscription: any;
  currencySubscription: any;
  firstlansub = false;
  userInfoSubmit = 0;
  constructor(private globals: Globals, private userservices: UserServices, private router: Router,
              private appservices: AppServices, public SS: SubscribersServices) {
    this.params = this.appservices.MSDFU(this.SS.isLocalSearchData.value);
    this.imageUrl = this.globals.IMAGE_URL;
    this.currency = JSON.parse(localStorage.getItem('localSearchData'));
    this.currency = this.currency.currency;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, showWeekNumbers: false, dateInputFormat: 'YYYY-MM-DD' });
    this.activeMenuItem = 0;
    this.userInfo = new UserInfo();
  }
  ngOnDestroy() {
    this.translateSubscription.unsubscribe();
    this.currencySubscription.unsubscribe();
  }
  ngOnInit() {
    this.translateSubscription = this.SS.isLocalSearchData.subscribe(() => {
      this.getUserBookings();
    });
    this.currencySubscription = this.SS.isLanguageCurrent.subscribe(() => {
      if (this.firstlansub) { this.firstlansub = false; } else { this.getUserBookings(); }
      this.userservices.getCountriesList().subscribe(res => {
        this.countriesList = res;
        for (let i = 0; i < this.countriesList.length; i++) {
          if (this.countriesList[i].id === 'UA' || this.countriesList[i].id === 'RU') {
            const temp = this.countriesList[i];
            this.countriesList.splice(i, 1);
            this.countriesList.unshift(temp);
          }
        }
      });
    });
    this.initials = new Initials();
    this.userservices.getUserInfo().subscribe(
      res => {
        this.userInfo.init(res);
        this.initials.init(this.userInfo);
      }, err => {if (err.status === 401) {
        localStorage.removeItem('user');
        this.router.navigate(['/']);
      }}
    );
  }
  activeMenu(num: number) {
    return num === this.activeMenuItem;
  }
  bookCancel(id: number) {
    this.userservices.bookCancel(id).subscribe(res => {
      const indexcancel = this.bookings.findIndex(i => i.id === id);
      this.bookings[indexcancel].status = 'CANCELD_BY_USER';
    });
  }
  buttonDisable(value: string) {
    return value === 'CANCELD_BY_USER' || value === 'CANCELD_BY_HOTEL' || value === 'NO_SHOW';
  }
  closeRoom(id: number, roomid: number) {
    this.userservices.bookEditCancelBookingRoom(id, roomid).subscribe(res => {
      this.bookInfoEdited = true;
      const indexcancel = this.bookInfo.rooms.findIndex(i => i.roomid === id);
      this.bookInfo.rooms[indexcancel].status = 'CANCELD_BY_USER';
    });
  }
  dateChanged(value, roomindex: number) {
    if (value !== null) {
      if (this.bookInfo.changed[roomindex] === -1 || this.bookInfo.changed[roomindex] === 1000) { this.bookInfo.changed[roomindex] = 0; }
      this.bookInfo.changed[roomindex]++;
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDatesEdit[1] = day;
      if (value > this.bookInfo.rooms[roomindex].checkOut ||
        (value.getDate() >= this.bookInfo.rooms[roomindex].checkOut.getDate()
          && value.getMonth() >= this.bookInfo.rooms[roomindex].checkOut.getMonth()
          && value.getFullYear() >= this.bookInfo.rooms[roomindex].checkOut.getFullYear())) {
        this.bookInfo.rooms[roomindex].checkOut = day;
      }
    }
  }
  dateChanged2(value, roomindex: number) {
    if (value !== null) {
      this.bookInfo.changed[roomindex]++;
    }
  }
  getBookInfo(id: number) {
    this.bookInfo = undefined;
    this.userservices.getBookInfo(id).subscribe(res => {
      this.bookInfo = res;
      this.bookInfo['changed'] = Array(this.bookInfo.rooms.length).fill(0);
      this.bookInfo.rooms.forEach((_, index) => {
        this.bookInfo.rooms[index].checkIn = this.userservices.decodeDate(this.bookInfo.rooms[index].checkIn);
        this.bookInfo.rooms[index].checkOut = this.userservices.decodeDate(this.bookInfo.rooms[index].checkOut);
      });
      this.bookInfoLoaded = true;
    }, err => {
      this.bookInfoLoaded = true;
    });
  }
  getUserBookings() {
    this.userservices.getUserBookings('token').subscribe( res => {
      this.bookings = res;
      this.bookings.forEach((item, index) => {
        this.bookings[index]['vertical'] = false;
        this.bookings[index].hotel.stars = Array(this.bookings[index].hotel.stars).fill(1);
        if (this.bookings[index].hotel.photos.length > 0) {
          const ava = this.bookings[index].hotel.photos.find(x => x.isAvatar === true);
          if (ava) {
            this.bookings[index].hotel['avatar'] = ava;
          } else {
            this.bookings[index].hotel['avatar'] = this.bookings[index].hotel.photos[0];
          }
        } else {
          this.bookings[index].hotel['avatar'] = {};
        }
        if (!this.bookings[index].hotel.bb) {
          this.bookings[index].hotel['avatar'] = this.globals.IMAGE_URL + this.bookings[index].hotel.id + '/' + this.bookings[index].hotel['avatar'].name;
          this.bookings[index]['currency'] = 'UAH';
          this.bookings[index]['roomCount'] = this.bookings[index].rooms.length;
          let summ = 0;
          item.rooms.forEach((ite, ind) => {
            ite.days.forEach((it, i) => {
              summ += it.price;
            });
          });
          this.bookings[index]['totalPrice'] = summ;
        } else {
          this.bookings[index].hotel['avatar'] = 'http://photos.hotelbeds.com/giata/bigger/' + this.bookings[index].hotel['avatar'].path;
          this.bookings[index].hotel.id += '_bb';
        }
      });
      this.loaded = true;
    }, err => {
      this.loaded = true;
    });
  }
  activateMenu(num: number) {
    this.userInfoSubmit = 0;
    if (num === 0) {
      this.getUserBookings();
    }
    this.activeMenuItem = num;
  }
  checkChar(event: any) {
    const k = event.charCode;
    if (!(
        (k === 8) || (k === 32) || (k === 45) ||
        (k > 64 && k < 91) || (k > 96 && k < 123) ||
        (k > 1071 && k < 1104) || (k > 1039 && k < 1072)
      )) {
      event.preventDefault();
    }
  }
  compareDates(status: string, date: Date) {
    if (status !== 'CANCELED_BY_HOTEL' && status !== 'CANCELD_BY_USER' && status !== 'NO_SHOW') {
      const day = new Date();
      day.setHours(0, 0, 0, 0);
      return day < date;
    } else {
      return false;
    }
  }
  findMinDate(arr: any) {
    let result = new Date(arr[0].date);
    arr.forEach(item => {
      const dat = new Date(item.date);
      if (result > dat) {
        result = dat;
      }
    });
    return result;
  }
  findMaxDate(arr: any) {
    let result = new Date(arr[0].date);
    arr.forEach(item => {
      const dat = new Date(item.date);
      if (result < dat) {
        result = dat;
      }
    });
    return result;
  }
  editModalClose() {
    if (this.bookInfoEdited) {
      this.getUserBookings();
    }
    this.bookInfoLoaded = false;
    this.bookInfoEdited = false;
  }
  initVertical(img, bookindex: number) {
    if (img.offsetWidth > 0 && img.children[0].children[0].offsetWidth > 0) {
      if (img.offsetWidth - img.children[0].children[0].offsetWidth >= 1) {
        this.bookings[bookindex].vertical = true;
      }
    }
  }
  onSubmit() {
    this.userInfoSubmit = 0;
    console.log(this.userInfo);
    this.userservices.postUserInfo(this.userInfo).subscribe(res => {
      this.initials.init(this.userInfo);
      this.userInfoSubmit = 1;
    }, err => {
      this.userInfoSubmit = 2;
    });
  }
  onSubmitEdition() {
    this.bookInfo.rooms.forEach((item, index) => {
      if (this.bookInfo.changed[index] > 2) {
        const data = {
          from: moment(item.checkIn).format('YYYY-MM-DD'),
          to: moment(item.checkOut).format('YYYY-MM-DD'),
        };
        this.userservices.bookEditBookingRoom(this.bookInfo.id, item.id, data).subscribe(res => {
          this.bookInfoEdited = true;
          this.bookInfo.changed[index] = 1000;
        }, err => {
          this.bookInfo.changed[index] = -1;
        });
      }
    });
  }
}
