export class UserInfo {
  image: string;
  name: string;
  surname: string;
  phone: string;
  email: string;
  gender: string;
  country: string;
  constructor() {
    this.image = '';
    this.name = '';
    this.gender = '';
    this.surname = '';
    this.email = '';
    this.phone = '';
    this.country = '';
  }
  public init(data: any) {
    this.surname = data.surname ? data.surname : '';
    this.name = data.name ? data.name : '';
    this.phone = data.phone ? data.phone : '';
    this.email = data.email ? data.email : '';
    this.gender = data.gender ? data.gender : '';
    this.image = data.image ? data.image : '';
    this.country = data.country ? data.country.id : '';
  }
}
export class Initials {
  name: string;
  surname: string;
  constructor() {
    this.name = '';
    this.surname = '';
  }
  init(data: UserInfo) {
    this.name = data.name;
    this.surname = data.surname;
  }
}
