import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {UserServices} from './user.services';
import {UserComponent} from './user.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';
import {BsDatepickerModule, ModalModule} from 'ngx-bootstrap';
import {HttpLoaderFactory} from '../app.module';
const routes: Routes = [ { path: '', component: UserComponent } ];
@NgModule({
  imports: [
    FormsModule,
    TextMaskModule,
    CommonModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    UserComponent
  ],
  providers: [
    UserServices
  ]
})
export class UserModule { }
