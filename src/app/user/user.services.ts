import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';

@Injectable()
export class UserServices extends BaseService {
  decodeDate(value: string): Date {
    const values = value.split('-');
    return new Date(Number(values[0]), Number(values[1]) - 1, Number(values[2]), 0, 0, 0, 0);
  }
  getBookInfo(id: number): Observable<any> {
    return this.get('bookings/' + id);
  }
  getBookInfo_bb(id: number): Observable<any> {
    return this.get('booking/' + id + '/bb');
  }
  getUserBookings(value: any): Observable<any> {
    return this.get('users/booking');
  }
  getUserInfo(): Observable<any> {
    return this.get('users');
  }
  postUserInfo(value: any): Observable<any> {
    return this.post('users/update', value);
  }
  bookCancel(id: number): Observable<any> {
    return this.post('bookings/' + id + '/cancel', null);
  }
  bookEditBookingRoom(id: number, roomid: number, data: any): Observable<any> {
    return this.post('bookings/' + id + '/rooms/' + roomid + '/change-dates', data);
  }
  bookEditCancelBookingRoom(id: number, roomid: number): Observable<any> {
    return this.post('bookings/' + id + '/rooms/' + roomid + '/cancel', null);
  }
  getCountriesList(): Observable<any> {
    return this.get('countries');
  }
}
