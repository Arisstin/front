import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {Room, SearchData} from './app.classes';
import {SubscribersServices} from './core/subscribers.services';
import {Globals} from './app.globals';
import {Meta, Title} from '@angular/platform-browser';
import {SeoServices} from './core/seo.services';
import {patchComponentDefWithScope} from '@angular/core/src/render3/jit/module';
import {TranslateService} from '@ngx-translate/core';
@Injectable()
export class AppServices {
  constructor(private subscribers: SubscribersServices,
              private globals: Globals,
              @Inject(DOCUMENT) private doc,
              private title: Title,
              private meta: Meta,
              private seo: SeoServices,
              private translate: TranslateService) {}
  encodeDate(date: Date): string {
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  }
  decodeDate(value: string): Date {
    const values = value.split('-');
    return new Date(Number(values[0]), Number(values[1]) - 1, Number(values[2]), 0, 0, 0, 0);
  }
  listenUrlParams(): void { // TODO: определение сео параметров
    let loopindex = 0;
    const datamod = this.getUrlParams(window.location.href);
    datamod.limit = this.globals.HOTEL_LIMIT;
    let localSearchData: SearchData = JSON.parse(localStorage.getItem('localSearchData'));
    if (!localSearchData) {
      localSearchData = new SearchData();
    } else {
      localSearchData.checkIn = new Date(localSearchData.checkIn);
      localSearchData.checkOut = new Date(localSearchData.checkOut);
    }
    const dateTypesArr = ['checkIn', 'checkOut'];
    const numberTypesArr = ['lat', 'lng', 'limit', 'offset'];
    const arrayTypesArr = ['hf', 'rf', 'stars', 'ht', 'pr'];
    if (datamod.rooms) {
      loopindex = Number(datamod.rooms);
      datamod.rooms = [];
      while (loopindex--) {
        datamod.rooms.push(new Room());
      }
      if (datamod.adults) {
        datamod.adults = datamod.adults.split(',');
        loopindex = datamod.adults.length;
        while (loopindex--) {
          const adult = datamod.adults[loopindex].split('-').map(Number);
          if (datamod.rooms[adult[0] - 1]) {
            datamod.rooms[adult[0] - 1].adults = adult[1];
          }
        }
      }
      if (datamod.children) {
        datamod.children = datamod.children.split(',');
        loopindex = datamod.children.length;
        while (loopindex--) {
          const child = datamod.children[loopindex].split('-').map(Number);
          if (datamod.rooms[child[0] - 1]) {
            datamod.rooms[child[0] - 1].c_ages.push(child[1]);
          }
        }
      }
    } else {
      datamod.rooms = [new Room()];
    }
    delete datamod.adults;
    delete datamod.children;
    const keys = Object.keys(datamod);
    keys.forEach(key => {
      if (arrayTypesArr.includes(key)) {
        if (typeof datamod[key] === 'string') {
          datamod[key] = [datamod[key]];
        }
        if (key !== 'pr') {
          datamod[key] = datamod[key].map(Number);
        }
      }
      if (numberTypesArr.includes(key)) {
        datamod[key] = Number(datamod[key]);
      }
      if (dateTypesArr.includes(key)) {
        datamod[key] = this.decodeDate(datamod[key]);
      }
      if (key === 'r_title') {
        datamod[key] = datamod[key].replace('+', ' ');
      }
      localSearchData[key] = datamod[key];
    });
    this.subscribers.isLocalSearchData.next(localSearchData);
  }
  MSDFU(data: SearchData): any {
    const dat = Object.assign({}, data);
    if (data.checkIn || data.checkOut) {
      dat.checkIn = this.encodeDate(dat.checkIn);
      dat.checkOut = this.encodeDate(dat.checkOut);
    }
    if (data.rooms) {
      const room = data.rooms.length;
      delete dat.rooms;
      let adulststring = '';
      let childrenstring = '';
      data.rooms.forEach((item, index) => {
        if (item.adults > 0) {
          adulststring += index + 1 + '-' + item.adults;
          adulststring += ',';
        }
        item.c_ages.forEach(ite => {
          childrenstring += index + 1 + '-' + ite;
          childrenstring += ',';
        });
      });
      childrenstring = childrenstring.slice(0, childrenstring.length - 1);
      adulststring = adulststring.slice(0, adulststring.length - 1);
      if (childrenstring !== '') { dat['children'] = childrenstring; }
      if (adulststring !== '') { dat['adults'] = adulststring; }
      dat['rooms'] = room;
    }
    return dat;
  }
  CSDTS(data: SearchData): URLSearchParams {
    const arrayKey = ['rf', 'ht', 'hf', 'stars', 'pr'];
    const dat = this.MSDFU(data);
    const params = new URLSearchParams();
    for (const key in dat) {
      if (arrayKey.includes(key)) {
        dat[key].forEach((item) => {
          params.append( key + '[]', item.toString());
        });
      } else {
        params.append(key, dat[key].toString());
      }
    }
    return params;
  }
  getUrlParams(url: string): any {
    const params = {};
    let loopindex = 0;
    let para = [];
    let par = url.split('?');
    if (par[1]) {
      par = par[1].split('&');
      loopindex = par.length;
      while (loopindex--) {
        if (par[loopindex] !== '') {
          para = par[loopindex].split('=');
          if (para[0] !== '' && para[1] !== '') {
            params[para[0]] = decodeURIComponent(para[1]);
          }
        }
      }
    }
    return params;
  }
  getSeoParams(): string {
    const params = window.location.href.split('?')[0].split('/');
    console.log(params);
    return '';
  }
  seoMod(data: SearchData, seo: string) {
    const dat = data;
    dat.seo = seo;
    delete dat.pr;
    delete dat.hf;
    delete dat.ht;
    delete dat.rf;
    delete dat.r_title;
    delete dat.r_id;
    delete dat.c_id;
    delete dat.lat;
    delete dat.lng;
    delete dat.checkIn;
    delete dat.checkOut;
    dat.offset = 0;
    return dat;
  }
  getDirectionBreadcrump(data: SearchData) {
    if (data.r_title) { return [data.r_title]; } else {
      if (data.seo) { return data.seo.split(','); } else {
        return ['breadcrumps_search'];
      }
    }
  }
  initLanguage(browserlan: string) {
    let language = 'en';
    const languages = ['ua', 'ru', 'en'];
    const url = window.location.href.split('/');
    if (url[3] && languages.includes(url[3])) {
      language = url[3];
    } else {
      const lang = localStorage.getItem('language');
      if (lang) {
        language = lang;
      } else {
        const lan = browserlan.split('-');
        if (languages.includes(lan[0])) {
          language = lan[0];
        } else {
          language = 'en';
        }
      }
    }
    this.subscribers.isLanguageCurrent.next(language);
    return language;
  }
  createLinkCanonical(value: string) {
    this.removeLinkCanonical();
    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');
    this.doc.head.appendChild(link);
    link.setAttribute('href', 'https://bronirovka.ua' + value);
  }
  removeLinkCanonical() {
    const canonical = this.doc.querySelector('link[rel="canonical"]');
    if (canonical) {
      this.doc.head.removeChild(canonical);
    }
  }
  initTitle(urls: string[]) {
    let title = 'ua';
    if (urls.includes('information')) {
      title = urls[3];
      this.translate.get(title).subscribe(res => { this.title.setTitle('Bronirovka.UA : ' + res); });
    } else {
      if (urls.includes('hotels')) {
        if (urls[3] && urls[3] !== 'hotel') {
          title = urls[3];
          if (urls[4] && urls[4] !== 'hotel') {
            title = urls[4];
          }
        } else {
          title = 'whole_world';
        }
        if (this.subscribers.isLanguageCurrent.value !== 'en') {
          title += '_p';
        }
        this.translate.get(title).subscribe(resa => {
          this.translate.get('main_title', {value: resa}).subscribe(res => {
            this.title.setTitle(res);
          });
        });
      } else {
        this.title.setTitle('Bronirovka.UA');
      }
    }
    if (urls.includes('hotel')) {
      this.translate.get('breadcrumps_hotel').subscribe(res => {
        this.title.setTitle(res);
      });
    }
    if (urls.includes('book') || urls.includes('book-hb')) {
      this.translate.get('breadcrumps_booking').subscribe(res => {
        this.title.setTitle(res);
      });
    }
    if (!urls[2]) {
      if (this.subscribers.isLanguageCurrent.value !== 'en') {
        title += '_p';
      }
      this.translate.get(title).subscribe(resa => {
        this.translate.get('main_title', {value: resa}).subscribe(res => {
          this.title.setTitle(res);
        });
      });
    }
  }
  initSeo(value: string, url: string) {
    this.createLinkCanonical(url);
    this.seo.getSeoInfo(value).subscribe(res => {
      const tag = this.meta.getTag('name="description"');
      if (tag) {
        this.meta.updateTag({content: res.meta}, 'name="description"');
      } else {
        this.meta.addTag({name: 'description', content: res.meta});
      }
      return res.description;
    });
  }
  deleteSeo() {
    this.meta.removeTag('name="description"');
    this.removeLinkCanonical();
  }
  sendGAevent(category: string, label: string, action: string, value ?: number) {
    (<any>window).ga('send', 'event', {
      eventCategory: category,
      eventLabel: label,
      eventAction: action,
      eventValue: value ? value : 0
    });
  }
}
