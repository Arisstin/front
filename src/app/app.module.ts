import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {Globals} from './app.globals';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {UrlSerializer, RouterModule, Routes} from '@angular/router';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule, BsDatepickerModule, ModalModule, BsLocaleService, CarouselModule, TooltipModule } from 'ngx-bootstrap';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ruLocale, enGbLocale, uaLocale } from 'ngx-bootstrap/locale';
import { AgmCoreModule } from '@agm/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './core/token.interceptor';
import { JwtInterceptor } from './core/jwt.interceptor';
import { LowerCaseUrlSerializer } from './core/routes-seo.services';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { BreadcrumpsComponent } from './breadcrumps/breadcrumps.component';
import { LocationlistComponent } from './search-form/locationlist/locationlist.component';
import { SearchComponent } from './search/search.component';
import { MainComponent } from './main/main.component';
import { HoteltableComponent } from './hotel/hoteltable.component/hoteltable.component';
import { HotelComponent } from './hotel/hotel.component';
import { SearchHotelComponent } from './search/search-hotel/search-hotel.component';
import { SearchFilterComponent } from './search/search-filter/search-filter.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { HotelServices } from './hotel/hotel.services';
import { SubscribersServices } from './core/subscribers.services';
import { HeaderServices } from './header/header.services';
import { SearchServices } from './search/search.services';
import { SearchFormServices } from './search-form/search-form.services';
import { AppServices } from './app.services';
import { NotfoundComponent } from './notfound/notfound.component';
import {SeoServices} from './core/seo.services';

defineLocale('ru', ruLocale);
defineLocale('en', enGbLocale);
defineLocale('ua', uaLocale);

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
export const routs: Routes = [
  {path: '', component: MainComponent},
  {path: ':lan', component: MainComponent},
  {path: ':lan/users/recovery', component: MainComponent},
  {path: '404', component: NotfoundComponent},
  {path: ':lan/hotels', component: SearchComponent},
  {path: ':lan/hotels/hotel/:id', component: HotelComponent},
  {path: ':lan/hotels/hotel/:id/book', loadChildren: './booking/booking.module#BookingModule'},
  {path: ':lan/hotels/hotel/:id/book-hb', loadChildren: './booking/booking-hb.module#BookingHbModule'},
  {path: ':lan/hotels/:country', component: SearchComponent},
  {path: ':lan/hotels/:country/hotel/:id', component: HotelComponent},
  {path: ':lan/hotels/:country/hotel/:id/book', loadChildren: './booking/booking.module#BookingModule'},
  {path: ':lan/hotels/:country/hotel/:id/book-hb', loadChildren: './booking/booking-hb.module#BookingHbModule'},
  {path: ':lan/hotels/:country/:city', component: SearchComponent},
  {path: ':lan/hotels/:country/:city/hotel/:id', component: HotelComponent},
  {path: ':lan/hotels/:country/:city/hotel/:id/book', loadChildren: './booking/booking.module#BookingModule'},
  {path: ':lan/hotels/:country/:city/hotel/:id/book-hb', loadChildren: './booking/booking-hb.module#BookingHbModule'},
  {path: ':lan/hotel/:id', component: HotelComponent},
  {path: ':lan/hotel/:id/book', loadChildren: './booking/booking.module#BookingModule'},
  {path: ':lan/hotel/:id/book-hb', loadChildren: './booking/booking-hb.module#BookingHbModule'},
  {path: ':lan/booking/:id/:bb', loadChildren: './booking/booking-table.module#BookingTableModule'},
  {path: ':lan/mylist', loadChildren: './mylist/mylist.module#MylistModule'},
  {path: ':lan/cabinet', loadChildren: './user/user.module#UserModule'},
  {path: ':lan/confirm/:token', loadChildren: './confirm-register/confirm-register.module#ConfirmRegisterModule'},
  {path: ':lan/information/:id', loadChildren: './information/information.module#InformationModule'},
  {path: '**', redirectTo: '/404', pathMatch: 'full'}
];
@NgModule({
  declarations: [
    HotelComponent,
    HoteltableComponent,
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    SearchComponent,
    SearchFormComponent,
    SearchFilterComponent,
    SearchHotelComponent,
    LocationlistComponent,
    BreadcrumpsComponent,
    NotfoundComponent,
  ],
  imports: [
    CarouselModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    RouterModule.forRoot(routs),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBSbgGePitwHSVOpd0xnq5dYJcXIhyjy-A'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    TranslateService,
    HotelServices,
    BsLocaleService,
    SubscribersServices,
    SearchServices,
    SearchFormServices,
    HeaderServices,
    AppServices,
    Globals,
    SeoServices,
    {
      provide: UrlSerializer,
      useClass: LowerCaseUrlSerializer
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
