import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';

@Injectable()
export class InformationServices extends BaseService {
  getInfoContent(value: string): Observable<any> {
    return this.get('information/' + value);
  }
}
