import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {InformationServices} from './information.services';
import {SubscribersServices} from '../core/subscribers.services';
import {AppServices} from '../app.services';

@Component ({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnDestroy {
  seo: string;
  content: any;
  subscriber: any;
  titles: any;
  constructor(private activeroute: ActivatedRoute, private infoservices: InformationServices, private SS: SubscribersServices, private appservice: AppServices) {
    this.seo = this.activeroute.snapshot.paramMap.get('id');
    this.infoservices.getInfoContent(this.seo).subscribe(res => {
      this.content = res;
    });
    this.appservice.initSeo(this.seo, '/' + this.SS.isLanguageCurrent.value + '/information/' + this.seo);
  }
  ngOnDestroy() {
    this.appservice.deleteSeo();
  }
}
