import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory} from '../app.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {RouterModule, Routes} from '@angular/router';
import {InformationComponent} from './information.component';
import {InformationServices} from './information.services';

const routes: Routes = [ { path: '', component: InformationComponent} ];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    InformationComponent
  ],
  providers: [
    InformationServices
  ]
})
export class InformationModule { }
