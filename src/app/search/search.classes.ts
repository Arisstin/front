export class Hotel {
  bb: boolean;
  id: string;
  title: string;
  rooms: Room[];
  photos: string[];
  location: HotelLocation;
  stars: any;
  pricewithdiscount: number;
  price: number;
  roomcount ?: number;
  adults ?: number;
  children ?: number;
  totalPrice: number;
}
export class ErrorMessage {
  property_path: string;
  message: string;
}
export class HotelLocation {
  lat: number;
  lng: number;
  address: string;
}
export class Room {
  price: string;
  pricewithdiscount: string;
  title: string;
  count: number;
  adults: any;
  children: any;
}
export class Filter {
  popFacilies: FilterItem[];
  hotelFacilies: FilterItem[];
  roomFacilies: FilterItem[];
  hotelTypes: FilterItem[];
  facilities: FilterItem[];
  stars: any[];
  priceRanges: Price[];
  constructor(data: any) {
    this.popFacilies = [];
    this.hotelFacilies = [];
    this.roomFacilies = [];
    this.priceRanges = data.priceRanges;
    this.hotelTypes = data.hotelTypes;
    this.facilities = data.facilities;
    this.stars = data.stars;
  }
}
export class FilterItem {
  count: number;
  id: number;
  type ?: string;
  belong_to ?: string;
  title: string;
  check ?: boolean;
}
export class Marker {
  id: string;
  lat: number;
  lng: number;
  title: string;
  price: number;
  stars: number[];
  constructor(data: Hotel) {
    this.id = data.id.toString();
    this.lat = data.location.lat;
    this.lng = data.location.lng;
    this.title = data.title;
    this.stars = data.stars ? new Array(data.stars).fill(1) : [];
    this.price = data.totalPrice;
  }
}
export class Price {
  from?: any;
  to?: any;
  count: number;
  check ?: boolean;
}
export class HotelsCount {
  count: number;
}
