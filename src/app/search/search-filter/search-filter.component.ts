import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {SearchData} from '../../app.classes';
import {Filter} from '../search.classes';
import {SubscribersServices} from '../../core/subscribers.services';
import {SearchServices} from '../search.services';
import {AppServices} from '../../app.services';
import {trigger, style, transition, animate, state} from '@angular/animations';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss'],
  animations: [
    trigger('extended', [
      state('show', style({height: '100%', opacity: '1', overflow: 'visible' })),
      state('hide', style({height: '0', opacity: '0', overflow: 'hidden' })),
      transition('hide <=> show', animate('200ms linear')),
    ])
  ]
})
export class SearchFilterComponent implements OnInit, OnDestroy {
  @Input() view: string;
  @Input() seoMod: boolean;
  @Output() filterstart = new EventEmitter<boolean>();
  @ViewChild('filter') filterMobile: ElementRef;
  hideMobileFixHead = true;
  searchData: SearchData;
  filterList: Filter;
  loaded = true;
  openedFacilies: any;
  hotelFaciliesList: string[];
  roomFaciliesList: string[];
  popularFacilies: number[];
  OpenHotelTypeFacil = false;
  subscribers = [];
  loopindex = 0;
  stars = Array(6).fill(1);
  sta = Array(5).fill(1);
  firstlansub = true;
  constructor(private subscribersservices: SubscribersServices, private searchservices: SearchServices,
              private appservices: AppServices) {
    this.popularFacilies = [1, 500, 501, 502];
    this.openedFacilies = {'BUSINESS' : false, 'CLEANING_SERVICE' : false, 'ENTERTAINMENT' : false, 'HEALTH_SERVICE' : false,
      'NUTRITION' : false, 'OTHER' : false, 'PUBLIC_PLACE' : false, 'RECEPTION' : false, 'SHOP' : false, 'SPORT' : false,
      'TRANSPORT' : false, 'AMENITIES' : false, 'BATHROOM' : false, 'FOREIGN_TERRITORY' : false, 'MEDIA' : false, 'KITCHEN' : false,
      'SERVICE' : false, 'GENERALHOTEL': false, 'GENERALROOM': false};
    this.roomFaciliesList = ['AMENITIES', 'MEDIA', 'KITCHEN', 'BATHROOM', 'FOREIGN_TERRITORY', 'SERVICE'];
    this.hotelFaciliesList = ['ENTERTAINMENT', 'CLEANING_SERVICE', 'OTHER', 'RECEPTION', 'TRANSPORT',
      'PUBLIC_PLACE', 'NUTRITION', 'BUSINESS', 'HEALTH_SERVICE', 'SPORT', 'SHOP'];
  }
  ngOnInit() {
    this.subscribers.push(this.subscribersservices.isLocalSearchData.subscribe(res => {
      this.searchData = res;
      this.loaded = false;
      this.load();
    }));
    this.subscribers.push(this.subscribersservices.isLanguageCurrent.subscribe(() => {
      if (this.firstlansub) { this.firstlansub = false; } else { this.loaded = false; this.load(); }
    }));
  }
  ngOnDestroy() {
    this.loopindex = this.subscribers.length;
    while (this.loopindex--) {
      this.subscribers[this.loopindex].unsubscribe();
    }
  }
  checkChecking(item: number, mod: string) {
    if (mod === 'ROOM') {
      return this.searchData.rf && this.searchData.rf.includes(item);
    } else {
      return this.searchData.hf && this.searchData.hf.includes(item);
    }
  }
  load() {
    this.searchservices.getSearhFilters(this.appservices.CSDTS(this.searchData)).subscribe(res => {
      if (res.stars) {
        this.filterList = new Filter(res);
        this.loopindex = this.filterList.stars.length;
        const starsarr = [];
        while (this.loopindex--) {
          starsarr.push({
            count: this.filterList.stars[this.loopindex],
            check: this.searchData.stars && this.searchData.stars.includes(this.loopindex)
          });
        }
        this.filterList.stars = starsarr;
        this.loopindex = this.filterList.hotelTypes.length;
        while (this.loopindex--) {
          this.filterList.hotelTypes[this.loopindex].check =
            this.searchData.ht && this.searchData.ht.includes(this.filterList.hotelTypes[this.loopindex].id);
        }
        this.loopindex = this.searchData.seo ? 0 : this.filterList.priceRanges.length;
        while (this.loopindex--) {
          if (this.searchData.pr) {
            this.filterList.priceRanges[this.loopindex].check =
              this.filterList.priceRanges[this.loopindex].to ?
                this.searchData.pr.includes(this.filterList.priceRanges[this.loopindex].from.toString() +
                  '-' + this.filterList.priceRanges[this.loopindex].to.toString()) :
                this.searchData.pr.includes(this.filterList.priceRanges[this.loopindex].from.toString());
          } else {
            this.filterList.priceRanges[this.loopindex].check = false;
          }
        }
        this.loopindex = this.filterList.facilities.length;
        while (this.loopindex--) {
          this.filterList.facilities[this.loopindex].check =
            this.checkChecking(this.filterList.facilities[this.loopindex].id, this.filterList.facilities[this.loopindex].belong_to);
          if (this.popularFacilies.includes(this.filterList.facilities[this.loopindex].id)) {
            this.filterList.popFacilies.push(this.filterList.facilities[this.loopindex]);
          } else {
            if (this.filterList.facilities[this.loopindex].belong_to === 'ROOM') {
              this.filterList.roomFacilies.push(this.filterList.facilities[this.loopindex]);
            } else {
              this.filterList.hotelFacilies.push(this.filterList.facilities[this.loopindex]);
            }
          }
        }
        const popularfind = this.filterList.popFacilies;
        this.filterList.popFacilies = [];
        this.popularFacilies.forEach(pop => {
          const ind = popularfind.findIndex(x => x.id === pop);
          if (ind > -1) {
            this.filterList.popFacilies.push(popularfind[ind]);
          }
        });
      } else {
        this.filterList = undefined;
      }
      this.loaded = true;
    }, () => {
      this.filterList = undefined;
      this.loaded = true;
    });
  }
  checkFacility(mod: string, item: any, index ?: number) {
    if (!this.searchData[mod]) { this.searchData[mod] = []; }
    if (mod === 'pr') {
      const find = item.to ? item.from.toString() + '-' + item.to.toString() : item.from.toString();
      const ind = this.searchData[mod].indexOf(find);
      if (ind === -1) { this.searchData[mod].push(find); } else { this.searchData[mod].splice(ind, 1); }
    } else {
      const ind = this.searchData[mod].indexOf(item);
      if (ind === -1) { this.searchData[mod].push(item); } else { this.searchData[mod].splice(ind, 1); }
    }
    if (this.searchData[mod].length === 0) { delete this.searchData[mod]; }
    if (this.view === 'mobile') {
      this.mobileClick(mod, index);
    } else {
      this.start();
    }
  }
  filterClear() {
    delete this.searchData.pr;
    delete this.searchData.ht;
    delete this.searchData.hf;
    delete this.searchData.rf;
    delete this.searchData.stars;
    this.start();
  }
  filterScroll() {
    this.hideMobileFixHead = !(this.filterMobile.nativeElement.getBoundingClientRect().top < 10 &&
      this.filterMobile.nativeElement.offsetHeight + this.filterMobile.nativeElement.getBoundingClientRect().top > 100);
  }
  mobileClick(mod: string, index: number) {
    switch (mod) {
      case 'ht': { this.filterList.hotelTypes[index].check = !this.filterList.hotelTypes[index].check; break; }
      case 'hf': { this.filterList.hotelFacilies[index].check = !this.filterList.hotelFacilies[index].check; break; }
      case 'rf': { this.filterList.roomFacilies[index].check = !this.filterList.roomFacilies[index].check; break; }
      case 'pr': { this.filterList.priceRanges[index].check = !this.filterList.priceRanges[index].check; break; }
      case 'stars': { this.filterList.stars[index].check = !this.filterList.stars[index].check; break; }
    }
  }
  start() {
    this.searchData.offset = 0;
    this.filterstart.emit(true);
    this.subscribersservices.isLocalSearchData.next(this.searchData);
  }
  state(value: boolean) { return value ? 'show' : 'hide'; }
}
