import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {SubscribersServices} from '../core/subscribers.services';
import {Links, SearchData, Link} from '../app.classes';
import {Globals} from '../app.globals';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchServices} from './search.services';
import {AppServices} from '../app.services';
import {Location} from '@angular/common';
import {ErrorMessage, Marker} from './search.classes';
import {BsModalService, BsModalRef} from 'ngx-bootstrap';
import {SeoServices} from '../core/seo.services';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  searchFormMod = 'search';
  displayScroll = false;
  hotelList: any[];
  hotelCount: number;
  loaders = new Array(2).fill(false);
  hotelsPreloading: number[];
  subscribers = [];
  searchData: SearchData;
  loopindex = 0;
  mapCenter: number[];
  mapMarkers: Marker[];
  modalRef: BsModalRef;
  errormessages: ErrorMessage[];
  menuM = [0, 0, 0];
  FLS = true;
  FSDS = true;
  pagination: number[] = [1];
  currentPage = 1;
  links: Links;
  seoMod = false;
  seoDescription: string = undefined;
  constructor(private SS: SubscribersServices,
              private seoS: SeoServices,
              private globals: Globals,
              private router: Router,
              private activeroute: ActivatedRoute,
              private searchservices: SearchServices,
              public appservices: AppServices,
              private location: Location,
              private modalservice: BsModalService) {
    console.log('constructor search');
    document.body.className = 'body-search';
    this.seachResize();
    this.hotelList = [];
    this.hotelsPreloading = new Array(this.globals.HOTEL_LIMIT).fill(1);
    this.subscribers.push(this.activeroute.url.subscribe(res => {
      console.log('url subscriber');
      console.log(res);
      this.links = new Links('breadcrumps_search');
      this.links.link = [];
      this.searchData = this.SS.isLocalSearchData.value;
      if (this.activeroute.snapshot.paramMap.get('country')) {
        this.seoMod = true;
        let seo = '';
        seo += this.activeroute.snapshot.paramMap.get('country');
        this.links.current = seo;
        if (this.activeroute.snapshot.paramMap.get('city')) {
          this.links.link.push(new Link(seo, seo));
          this.links.current = this.activeroute.snapshot.paramMap.get('city');
          seo += ',' + this.activeroute.snapshot.paramMap.get('city');
        }
        this.searchData = this.appservices.seoMod(this.SS.isLocalSearchData.value, seo);
        this.appservices.initSeo(this.searchData.seo.split(',')[0], this.router.url.split('?')[0]);
      } else {
        this.seoMod = false;
        if (this.searchData.r_title) { this.links.current = this.searchData.r_title; }
      }
        this.seoD();
      this.SS.isLocalSearchData.next(this.searchData);
    }));
  }
  ngOnInit() {
    this.subscribers.push(this.SS.isLocalSearchData.subscribe(res => {
        console.log('Search Data subscriber');
     // if (this.FSDS) { this.FSDS = false; } else {
        this.searchData = res;
        this.clearPagination();
        this.loadHotels();
     // }
    }));
    this.subscribers.push(this.SS.isLanguageCurrent.subscribe(() => {
      console.log('Language subscriber');
      if (this.FLS) { this.FLS = false; } else {
        this.seoD();
        this.loadHotels(true);
      }
    }));
  }
  ngOnDestroy() {
    document.body.className = '';
    this.appservices.deleteSeo();
    this.loopindex = this.subscribers.length;
    while (this.loopindex--) {
      this.subscribers[this.loopindex].unsubscribe();
    }
  }
  seoD() {
    this.seoS.getSeoInfo(this.links.current).subscribe(res => { this.seoDescription = res.description; },
            err => { this.seoDescription = undefined; });
  }
  filterStart() {
    this.menuM.fill(0);
  }
  onSearch(event: SearchData) {
    this.seoMod = false;
    this.router.navigate( [this.SS.isLanguageCurrent.value, 'hotels'], {queryParams: this.appservices.MSDFU(event) });
    if (this.links.current !== event.r_title) {
      this.links.current = event.r_title;
    }
  }
  onMouseOverMarker(infoWindow, gm) {
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }
    gm.lastOpen = infoWindow;
    infoWindow.open();
  }
  openMap(template: TemplateRef<any>, event ?: any) {
    this.mapMarkers = [];
    if (event) {
      this.loaders[1] = true;
      const hotel = this.hotelList.find(x => x.id === event);
      this.mapMarkers.push(new Marker(hotel));
      this.mapCenter = [hotel.location.lat, hotel.location.lng];
      this.modalRef = this.modalservice.show(template);
      this.loaders[1] = false;
    } else {
      this.loaders[1] = true;
      const searchparams = this.SS.isLocalSearchData.value;
      delete searchparams.offset;
      delete searchparams.limit;
      this.mapCenter = [searchparams.lat, searchparams.lng];
      this.searchservices.getSearchResult(this.appservices.CSDTS(searchparams)).subscribe(res => {
        if (res.result) {
          res.result.forEach(item => {
            this.mapMarkers.push(new Marker(item));
          });
        }
        this.modalRef = this.modalservice.show(template);
        this.loaders[1] = false;
      });
    }
  }
  loadHotels(mod ?: boolean) {
    if (!mod) { this.loaders[0] = true; }
    this.location.go(this.router.createUrlTree([this.router.url.split('?')[0]],
      { queryParams: this.appservices.MSDFU(this.searchData) }).toString());
    this.searchservices.getSearchResult(this.appservices.CSDTS(this.searchData)).subscribe(res => {
      if (res.result) {
        this.hotelCount = res.count;
        this.pagination = this.hotelCount > 0 ? Array(Math.ceil(this.hotelCount / this.globals.HOTEL_LIMIT)).fill(1) : [];
        this.hotelList = res.result;
        this.loaders[0] = false;
        this.errormessages = undefined;
      } else {
        this.hotelCount = undefined;
        this.hotelList = [];
        this.errormessages = res;
        this.loaders[0] = false;
      }
    }, () => {
      this.hotelList = [];
      this.loaders[0] = false;
      this.errormessages = [];
    });
  }
  sortHotels(key: string, mod: string) {
    this.menuM.fill(0);
    delete this.searchData.order_by_price;
    delete this.searchData.order_by_stars;
    this.searchData.offset = 0;
    this.searchData[key] = mod;
    this.currentPage = 0;
    this.loadHotels(true);
  }
  setPage(page: number) {
    if (this.currentPage !== page) {
      scrollTo(0, 0);
      this.searchData.offset = page * this.globals.HOTEL_LIMIT;
      this.currentPage = page;
      this.loadHotels(true);
    }
  }
  clearPagination() {
      this.currentPage = 0;
  }
  openMenuM(i: number) {
    if (this.menuM[i]) { this.menuM.fill(0); } else { this.menuM.fill(0); this.menuM[i] = 1; }
    if (this.menuM[0]) {
      setTimeout(() => { this.menuM[0] = 0; }, 1000);
    }
  }
  openHotel(data: any) {
    this.router.navigate( ['/' + this.SS.isLanguageCurrent.value, 'hotel', data.hotelId],
      { queryParams: this.appservices.MSDFU(data.data) });
  }
  searchScroll() {
    this.displayScroll = document.documentElement.scrollTop > 1500;
  }
  scrollToTop() {
    window.scroll(0, 0);
  }
  seachResize() {
    this.searchFormMod = window.innerWidth < 768 ? 'mobile' : 'search';
  }
}
