import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Globals} from '../../app.globals';
import {SearchData} from '../../app.classes';
import {AppServices} from '../../app.services';
import {SearchServices} from '../search.services';
import {SubscribersServices} from '../../core/subscribers.services';
import {trigger, style, transition, animate, state} from '@angular/animations';
import {Hotel} from '../search.classes';


@Component({
  selector: 'app-search-hotel',
  templateUrl: './search-hotel.component.html',
  styleUrls: ['./search-hotel.component.scss'],
  animations: [
    trigger('showExtendedRooms', [
      state('show', style({
        height: '*',
        opacity: '1',
        overflow: 'hidden' })),
      state('hide', style({
        height: '0',
        opacity: '0',
        overflow: 'hidden' })),
      state('open', style({ transform: 'rotate(90deg)', top: '13px' })),
      state('close', style({ top: '11px' })),
      transition('hide <=> show', animate('300ms linear')),
      transition('open <=> close', animate('300ms linear'))
    ])
  ]
})
export class SearchHotelComponent implements OnInit {
  @Input() hotel: Hotel;
  @Output() openMap = new EventEmitter<string>();
  showExtended = false;
  loadingExtended = true;
  roomExtendedList: any[];
  searchData: SearchData;
  imgheight = '100%';
  routeparams: any;
  photosInit: boolean[];
  photosPag: boolean[];
  nights = 0;
  constructor(private globals: Globals,
              private appservices: AppServices,
              private searchservices: SearchServices,
              public subscribersservices: SubscribersServices) {
    this.searchData = this.subscribersservices.isLocalSearchData.value;
    const day1 = new Date(this.searchData.checkIn);
    const day2 = new Date(this.searchData.checkOut);
    day1.setHours(0, 0, 0, 0);
    day2.setHours(0, 0, 0, 0);
    this.nights = Math.ceil((+day2 - +day1) / 86400000);
    this.routeparams = this.appservices.MSDFU(this.searchData);
  }
  ngOnInit() {
    if (!this.hotel.photos) { this.hotel.photos = ['../../../assets/images/hotel-thumb-default.jpg']; } else {
        this.hotel.photos = this.hotel.photos.map(path => {
            return this.hotel.bb ? 'http://photos.hotelbeds.com/giata/bigger/' + path : this.globals.IMAGE_URL + this.hotel.id + '/' + path;
        });
    }
    this.photosInit = Array(this.hotel.photos.length).fill(false);
    this.photosPag = Array(this.hotel.photos.length).fill(false);
    this.photosPag[0] = true;
    this.hotel.stars = this.hotel.stars ? Array(this.hotel.stars).fill(1) : [];
    this.hotel.adults = 0;
    this.hotel.children = 0;
    if (this.hotel.rooms) {
      this.hotel.roomcount = 0;
      let roomlenght = this.hotel.rooms.length;
      while (roomlenght--) {
        this.hotel.roomcount += this.hotel.rooms[roomlenght].count;
        this.hotel.rooms[roomlenght].adults = Array(this.hotel.rooms[roomlenght].adults).fill(1);
        this.hotel.rooms[roomlenght].children = Array(this.hotel.rooms[roomlenght].children).fill(1);
        this.hotel.adults += this.hotel.rooms[roomlenght].adults.length * this.hotel.rooms[roomlenght].count;
        this.hotel.children += this.hotel.rooms[roomlenght].children.length * this.hotel.rooms[roomlenght].count;
      }
    }
  }
  openmap() {
    this.openMap.emit(this.hotel.id);
  }
  initVertical(img, index) {
    if (img.offsetWidth > 0 && img.children[0].children[index].offsetWidth > 0) {
      this.imgheight = img.children[0].children[index].offsetHeight + 'px';  // TODO: пересчет при преодолении рубежа сетки
      if (img.offsetWidth - img.children[0].children[index].offsetWidth >= 1) {
        this.photosInit[index] = true;
        this.imgheight = '100%';
      }
      if (img.offsetHeight - img.children[0].children[index].offsetHeight > 1) {
        this.photosInit[index] = false;
        this.imgheight = '100%';
      }
    }
   // console.log(this.hotel.id, index, '|', img.offsetWidth, img.children[0].children[index].offsetWidth, '|', img.offsetHeight, img.children[0].children[index].offsetHeight );
  }
  photoSelect(index: number) {
    this.photosPag = Array(this.hotel.photos.length).fill(false);
    this.photosPag[index] = true;
  }
  loadExtended() {
    this.showExtended = !this.showExtended;
    if (this.showExtended) {
      this.loadingExtended = false;
      this.searchservices.getHotelRooms(this.hotel.id.toString(), this.appservices.CSDTS(this.searchData)).subscribe( res => {
        this.roomExtendedList = res;
        this.roomExtendedList.forEach((room, index) => {
          this.roomExtendedList[index]['mansArray'] = Array(room.tariffs[0].guest).fill(1);
          this.roomExtendedList[index]['childArray'] = Array(room.tariffs[0].children).fill(1);
        });
        this.loadingExtended = true;
      }, () => {
        this.loadingExtended = true;
        this.showExtended = false;
      });
    }
  }
  stateCaret() {
    return this.showExtended ? 'open' : 'close';
  }
  stateExtended() {
    return this.showExtended && this.loadingExtended && this.roomExtendedList ? 'show' : 'hide';
  }
}
