import { Injectable } from '@angular/core';
import { BaseService } from '../core/base.services';
import { Observable} from 'rxjs';
import {Filter, HotelsCount, Room} from './search.classes';

@Injectable()
export class SearchServices extends BaseService {
  getSearchCount(value: URLSearchParams): Observable<HotelsCount> {
    return this.get('count-hotels?' + value);
  }
  getSearchResult(value: URLSearchParams): Observable<any> {
    return this.get('search?' + value);
  }
  getSearchSeoResult(value: URLSearchParams): Observable<any> {
    return this.get('hotels?' + value);
  }
  getSearhFilters(value: URLSearchParams): Observable<Filter> {
    return this.get('filters?' + value);
  }
  getHotelRooms(hotel_id: string, url: URLSearchParams): Observable<Room[]> {
    return this.get('hotel/' + hotel_id + '/prices-for-period?' + url);
  }
}
