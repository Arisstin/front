import { Component } from '@angular/core';
import {SubscribersServices} from '../core/subscribers.services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  constructor(public SS: SubscribersServices) {
  }
}
