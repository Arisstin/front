import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingHbComponent } from './booking-hb/booking-hb.component';
import { BookingHbServices } from './booking-hb/booking-hb.services';
import { RouterModule, Routes } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../app.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';

const routes: Routes = [ { path: '', component: BookingHbComponent} ];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    BookingHbComponent
  ],
  providers: [
    BookingHbServices
  ]
})
export class BookingHbModule { }
