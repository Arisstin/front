import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingtableServices } from './bookingtable/bookingtable.services';
import { BookingtableComponent } from './bookingtable/bookingtable.component';
import { RouterModule, Routes } from '@angular/router';
import {HttpLoaderFactory} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
const routes: Routes = [ { path: '', component: BookingtableComponent} ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    BookingtableComponent
  ],
  providers: [
    BookingtableServices
  ]
})
export class BookingTableModule {}
