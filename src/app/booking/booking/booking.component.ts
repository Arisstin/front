import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {SavedData, LiqpayInfo} from '../booking.classes';
import {UserInfo} from '../../user/user.classes';
import {BookingServices} from './booking.services';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../../app.globals';
import {SubscribersServices} from '../../core/subscribers.services';
import {HotelServices} from '../../hotel/hotel.services';
import {AppServices} from '../../app.services';
import {Link, Links} from '../../app.classes';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, OnDestroy {
  confirmBooking = false;
  bookinginfo: any;
  bookdata: any;
  hotelInformation: any;
  bookingUrl: string;
  avatarPhoto: any;
  stars: number[];
  isLoaded = false;
  bookingForm: FormGroup;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  bookingPaymentsInfo: any;
  bookingRoomInfo: any;
  bookingTariffsCount: any;
  roomsCount = 0;
  roomsArray: SavedData;
  nutritionPrice = 0;
  roomsPrice = 0;
  location: any;
  hotelId: string;
  mask = ['+', /\d/, /\d/, ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskCard = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskCvc = [/\d/, /\d/, /\d/];
  postBookingInfo: any;
  postWrongBookingInfo: any[];
  nights = 0;
  userInfo = new UserInfo();
  translateSubscriptionInfo: any;
  liqpayInfo: LiqpayInfo;
  links: Links;
  params: any;
  constructor(private activeroute: ActivatedRoute,
              public bookingservice: BookingServices,
              private hotelservice: HotelServices,
              private globals: Globals,
              private appservices: AppServices,
              public SS: SubscribersServices,
              private router: Router) {
    this.bookinginfo = this.SS.isLocalSearchData.value;
    this.hotelId = this.activeroute.snapshot.paramMap.get('id');
    this.params = this.appservices.MSDFU(this.bookinginfo);
    const country = this.activeroute.snapshot.paramMap.get('country');
    const city = this.activeroute.snapshot.paramMap.get('city');
    if (country) {
      this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels', country], country));
      if (city) {
        this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels', country, city], city));
      }
    } else {
      this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels'],
        this.appservices.getDirectionBreadcrump(this.bookinginfo)[0]));
    }
    this.links.link.push(new Link(['../'], 'breadcrumps_hotel', this.hotelId));
    this.loadAll();
  }
  ngOnDestroy() {
    this.translateSubscriptionInfo.unsubscribe();
  }
  ngOnInit() {
    this.translateSubscriptionInfo = this.SS.isLanguageCurrent.subscribe(() => {
      this.hotelservice.getHotelInfo(this.hotelId).subscribe(result => {
        this.hotelInformation = result;
        this.stars = this.hotelInformation.stars ? Array(this.hotelInformation.stars).fill(1) : [];
        const avatara = this.hotelInformation.photos.find(function (photo) {
          return photo.isAvatar === true;
        });
        if (avatara) { this.avatarPhoto = avatara; }
        if (!avatara && this.hotelInformation.photos && this.hotelInformation.photos.length > 0) {
          this.avatarPhoto = this.globals.IMAGE_URL + this.hotelId + '/' + this.hotelInformation.photos[0].name;
        } else {
          this.avatarPhoto = '../../../assets/images/hotel-thumb-default.jpg';
        }
      });
    });
    this.bookingservice.getUserInfo().subscribe(resa => {
      this.userInfo.init(resa);
      this.initform();
    }, err => { this.initform(); });
  }
  lowcase() {
    this.bookingForm.patchValue({email: this.bookingForm.value.email.toLowerCase(), emailconfirm: this.bookingForm.value.emailconfirm.toLowerCase()});
  }
  loadAll() {
      this.bookdata = JSON.parse(localStorage.getItem('bookingInfo'));
      this.nights = (+this.bookinginfo.checkOut.setHours(0, 0, 0, 0) - +this.bookinginfo.checkIn.setHours(0, 0, 0, 0)) / 86400000;
      this.bookingTariffsCount = this.bookingservice.getTariffsCount(this.bookdata.tariffs);
      this.roomsCount = this.bookingservice.getRoomsCount(this.bookingTariffsCount);
      this.bookingservice.getBookingInformation(this.appservices.CSDTS(this.bookinginfo) +
        this.bookingservice.TariffsDataToString(this.bookdata.tariffs)).subscribe(results => {
        this.bookingRoomInfo = results.tariffs;
        this.bookingPaymentsInfo = results.payments;
        if (!this.bookingPaymentsInfo) {
          this.bookingPaymentsInfo = {isCardRequired: false};
        }
        this.roomsArray = this.bookingservice.roomsCreateArray(this.bookingRoomInfo, this.bookingTariffsCount, this.bookinginfo.adults);
        this.nutritionPrice = this.bookingservice.countNutririon(this.roomsArray);
        this.roomsPrice = this.bookingservice.countRoomsPrice(this.roomsArray);
      });
  }
  transformedBed(beds: any[]) {
    return beds.find(x => x.type.isTransformed === true);
  }
  initform() {
    this.bookingForm = new FormGroup({
      'work': new FormControl(''),
      'gender': new FormControl(this.userInfo.gender),
      'firstname': new FormControl(this.userInfo.name, [Validators.required]),
      'email': new FormControl(this.userInfo.email, [Validators.pattern(this.emailPattern), Validators.required]),
      'emailconfirm': new FormControl(this.userInfo.email, [Validators.pattern(this.emailPattern), Validators.required]),
      'surname': new FormControl(this.userInfo.surname, [Validators.required]),
      'country': new FormControl(this.userInfo.country),
      'phone': new FormControl(this.userInfo.phone, [Validators.minLength(18)]),
      'description': new FormControl(''),
      'creditcard': new FormControl('', [Validators.required, Validators.minLength(19)]),
      'cvv2': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'cardholder': new FormControl('', [Validators.required]),
      'exp_month': new FormControl('', [Validators.required]),
      'exp_year': new FormControl('', [Validators.required])
    }, {validators: this.matchingEmails('email', 'emailconfirm')});
  }
  matchingEmails(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const pass = group.controls[passwordKey];
      const confirm = group.controls[confirmPasswordKey];

      if (pass.value !== confirm.value) {
        return {
          mismatchedEmails: true
        };
      }
    };
  }

  get firstname() { return this.bookingForm.get('firstname'); }
  get surname() { return this.bookingForm.get('surname'); }
  get email() { return this.bookingForm.get('email'); }
  get emailconfirm() { return this.bookingForm.get('emailconfirm'); }
  get gender() { return this.bookingForm.get('gender'); }
  get country() { return this.bookingForm.get('country'); }
  get description() { return this.bookingForm.get('description'); }
  get phone() { return this.bookingForm.get('phone'); }
  get creditcard() { return this.bookingForm.get('creditcard'); }
  get cvv2() { return this.bookingForm.get('cvv2'); }
  get cardholder() { return this.bookingForm.get('cardholder'); }
  get exp_month() { return this.bookingForm.get('exp_month'); }
  get exp_year() { return this.bookingForm.get('exp_year'); }
  checkChar(event: any) {
    const k = event.charCode;
    if (!(
        (k === 8) || (k === 32) || (k === 45) ||
        (k > 64 && k < 91) || (k > 96 && k < 123) ||
        (k > 1071 && k < 1104) || (k > 1039 && k < 1072)
      )) {
      event.preventDefault();
    }
  }
  confirmingBooking() {
    if (!this.bookingForm.hasError('mismatchedEmails') &&
      this.surname.valid && this.firstname.valid && this.email.valid && this.emailconfirm.valid) {
      this.roomsArray.tariffs.forEach((item, i) => {
        item.rooms.forEach((_item, j) => {
          console.log('person', this.roomsArray.tariffs[i].rooms[j].person);
          if (this.roomsArray.tariffs[i].rooms[j].person.name === '') {
            this.roomsArray.tariffs[i].rooms[j].person.name = this.firstname.value;
          }
          if (this.roomsArray.tariffs[i].rooms[j].person.surname === '') {
            this.roomsArray.tariffs[i].rooms[j].person.surname = this.surname.value;
          }
        });
      });
      this.confirmBooking = true;
    } else {
      this.bookingForm.get('firstname').markAsTouched({onlySelf: true});
      this.bookingForm.get('surname').markAsTouched({onlySelf: true});
      this.bookingForm.get('email').markAsTouched({onlySelf: true});
      this.bookingForm.get('emailconfirm').markAsTouched({onlySelf: true});
    }
  }
  closeRoom(tariffid: number, tariffindex: number, roomindex: number) {
    this.bookingTariffsCount[tariffid].splice(roomindex, 1);
    if (this.bookingTariffsCount[tariffid].length === 0) {
      delete this.bookingTariffsCount[tariffid];
    }
    this.roomsArray.tariffs[tariffindex].rooms.splice(roomindex, 1);
    if (this.roomsArray.tariffs[tariffindex].rooms.length === 0) {
      this.bookingRoomInfo.splice(tariffindex, 1);
      this.roomsArray.tariffs.splice(tariffindex, 1);
      this.bookinginfo.beds.splice(tariffindex, 1);
    }
  }
  nutritionCheck(i: number, j: number, k: number) {
    if (this.roomsArray.tariffs[i].rooms[j].nutritioncheck[k]) {
      this.roomsArray.tariffs[i].rooms[j].nutritioncheck.fill(false);
      this.roomsArray.tariffs[i].rooms[j].nutritioncheck[k] = true;
      this.roomsArray.tariffs[i].rooms[j].nutrition = this.bookingRoomInfo[i].nutrition[k].price;
      this.roomsArray.tariffs[i].rooms[j].nutritionid = this.bookingRoomInfo[i].nutrition[k].id;
    } else {
      this.roomsArray.tariffs[i].rooms[j].nutrition = 0;
      this.roomsArray.tariffs[i].rooms[j].nutritionid = '';
    }
  }
  postBooking() {
    if (this.bookingForm.invalid && (this.bookingPaymentsInfo.isCardRequired || !this.confirmBooking || this.phone.invalid)) {
      this.bookingForm.get('phone').markAsTouched({onlySelf: true});
      if (this.bookingPaymentsInfo.isCardRequired) {
        this.bookingForm.get('creditcard').markAsTouched({onlySelf: true});
        this.bookingForm.get('cvv2').markAsTouched({onlySelf: true});
        this.bookingForm.get('cardholder').markAsTouched({onlySelf: true});
        this.bookingForm.get('exp_month').markAsTouched({onlySelf: true});
        this.bookingForm.get('exp_year').markAsTouched({onlySelf: true});
      }
    } else {
      const bookdata: any = {};
      bookdata['period'] = {
        'from': this.bookinginfo.checkIn,
        'to': this.bookinginfo.checkOut
      };
      bookdata['additional'] = {
        'comment': this.bookingForm.value.description,
        'country': this.bookingForm.value.country,
        'goal': this.bookingForm.value.work,
        'gender': this.bookingForm.value.gender,
      };
      bookdata['user'] = {
        'name': this.bookingForm.value.firstname,
        'surname': this.bookingForm.value.surname,
        'email': this.bookingForm.value.email,
        'phone': this.bookingForm.value.phone,
      };
      bookdata['tariffs'] = [];
      this.roomsArray.tariffs.forEach((item, i) => {
        item.rooms.forEach((_item, j) => {
          bookdata['tariffs'].push({
            'id': _item.id,
            'guest': _item.person.name + ' ' + _item.person.surname,
            'guestCount': Number(_item.people),
            'nutrition': {
              'id': _item.nutritionid,
              'guest': Number(_item.peoplenutrition),
            },
            'bed': this.bookdata.beds[i]
          });
        });
      });
      if (this.bookingPaymentsInfo.isCardRequired) {
        this.bookingservice.getLiqPayInfo(bookdata).subscribe(res => {
          this.liqpayInfo = res;
          window.location.href = 'https://www.liqpay.ua/api/3/checkout?data=' + this.liqpayInfo.data +
            '&signature=' + this.liqpayInfo.signature;
        }, err => {
          this.postWrongBookingInfo = [];
          if (err.status === 409) {
            this.postWrongBookingInfo = err.error;
            console.log(this.postWrongBookingInfo);
          }
        });
      } else {
        this.bookingservice.postBookData(bookdata).subscribe(res => {
          this.postBookingInfo = res;
          this.router.navigate(['/booking', this.postBookingInfo.id]);
        }, err => {
          this.postWrongBookingInfo = [];
          if (err.status === 409) {
            this.postWrongBookingInfo = err.error;
            console.log(this.postWrongBookingInfo);
          }
        });
      }
    }
    console.log(this.roomsArray);
  }
}
