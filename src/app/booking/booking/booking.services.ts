import { Injectable } from '@angular/core';
import { BaseService } from '../../core/base.services';
import { Observable} from 'rxjs';
import {BookingData, SavedData, Tariff, Room} from '../booking.classes';

@Injectable()
export class BookingServices extends BaseService {
  getBookingInformation(value: string): Observable<BookingData> {
    return this.get('booking-data?' + value);
  }
  getUserInfo(): Observable<any> {
    return this.get('users');
  }
  getLiqPayInfo(data: any): Observable<any> {
    return this.post('bb/book', data);
  }
  TariffsDataToString(data: any[]) {
    let str = '';
    data.forEach(item => {
      str += '&' + 'tariffs[' + item.id + ']=' + item.count;
    });
    return str;
  }
  getTariffsCount(data: any[]) {
    const array = {};
    data.forEach(item => {
      array[item.id] = Array(item.count).fill(1);
    });
    return array;
  }
  getRoomsCount(data: any) {
    let num = 0;
    for (const key in data) {
      num += data[key].length;
    }
    return num;
  }
  roomsCreateArray(data: any, count: any, people: any) {
    const roomsArray = new SavedData;
    roomsArray.tariffs = [];
    data.forEach((item, i) => {
      roomsArray.tariffs.push(new Tariff);
      roomsArray.tariffs[i].rooms = [];
      count[item.id].forEach((_item, j) => {
        roomsArray.tariffs[i].rooms.push(new Room);
        roomsArray.tariffs[i].rooms[j].id = item.id;
        roomsArray.tariffs[i].rooms[j].price.push(
          0,
          item.singleOccupancyPrice,
          item.doubleOccupancyPrice,
          item.tripleOccupancyPricePercent,
          item.fourOccupancyPricePercent,
          item.fiveOccupancyPricePercent);
        item.nutrition.forEach(_item_ => {
          roomsArray.tariffs[i].rooms[j].nutritioncheck.push(false);
          roomsArray.tariffs[i].rooms[j].people = people;
          roomsArray.tariffs[i].rooms[j].peoplenutrition = people;
        });
      });
    });
    return roomsArray;
  }
  countNutririon(data: SavedData) {
    let summ = 0;
    if (data) {
      data.tariffs.forEach(item => {
        item.rooms.forEach(_item => {
          summ += _item.nutrition * _item.peoplenutrition;
        });
      });
    }
    return summ;
  }
  countRoomsPrice(data: SavedData) {
    let summ = 0;
    if (data) {
      data.tariffs.forEach(item => {
        item.rooms.forEach(_item => {
          summ += _item.price[_item.people];
        });
      });
    }
    return summ;
  }
  postBookData(data: any): Observable<any> {
    return this.post('book', data);
  }
}
