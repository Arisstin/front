import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingComponent } from './booking/booking.component';
import { BookingServices } from './booking/booking.services';
import { RouterModule, Routes } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {HttpLoaderFactory} from '../app.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';

const routes: Routes = [ { path: '', component: BookingComponent} ];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    BookingComponent
  ],
  providers: [
    BookingServices
  ]
})
export class BookingModule { }
