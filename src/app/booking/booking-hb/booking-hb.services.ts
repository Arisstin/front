import { Injectable } from '@angular/core';
import { BaseService } from '../../core/base.services';
import { Observable} from 'rxjs';
import {SearchData} from '../../app.classes';

@Injectable()
export class BookingHbServices extends BaseService {
  getLiqPayInfo(data: any): Observable<any> {
    return this.post('bb/book', data);
  }
  getUserInfo(): Observable<any> {
    return this.get('users');
  }
  getBookingRoomTariffComment_hb(id: string): Observable<any> {
    return this.get('tariff/' + id + '/comments');
  }
  roomsCreateArray_hb(data: any) {
    const roomsArray = [];
    data.tariffs.forEach((item, ind) => {
      for (let i = 0; i < item._checked_count; i++) {
        roomsArray.push(Object.assign({}, item));
        roomsArray[roomsArray.length - 1]['person'] = { name: '', surname: ''};
        roomsArray[roomsArray.length - 1]['childAges'] = new Array(roomsArray[roomsArray.length - 1].children).fill(0);
        const room = data.rooms.find(x => x.adults === roomsArray[roomsArray.length - 1].guests.length &&
          x.c_ages.length === roomsArray[roomsArray.length - 1].childrens.length);
        if (room) {
          roomsArray[roomsArray.length - 1]['childAges'] = room.c_ages.map(Number);
        }
      }
    });
    return roomsArray;
  }
  countRoomsPrice_hb(data: any) {
    let summ = 0;
    data.forEach(item => {
      summ += Number(item.priceWithDiscount);
    });
    return summ;
  }
  countRoomsPrice_hb_uah(data: any) {
    let summ = 0;
    data.forEach(item => {
      summ += Number(item.baseCcyPrice);
    });
    return summ;
  }
}
