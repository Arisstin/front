import {Component, OnDestroy, OnInit} from '@angular/core';
import {Country, LiqpayInfo} from '../booking.classes';
import {UserInfo} from '../../user/user.classes';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {HotelServices} from '../../hotel/hotel.services';
import {BookingHbServices} from './booking-hb.services';
import {Globals} from '../../app.globals';
import {SubscribersServices} from '../../core/subscribers.services';
import {Links, SearchData, Link} from '../../app.classes';
import {ActivatedRoute} from '@angular/router';
import {AppServices} from '../../app.services';

@Component({
  selector: 'app-booking-hb',
  templateUrl: './booking-hb.component.html',
  styleUrls: ['./booking-hb.component.scss']
})
export class BookingHbComponent implements OnInit, OnDestroy {
  hotelid: string;
  counties = new Country();
  confirmBooking = false;
  bookinginfo: SearchData;
  hotelInformation: any;
  avatarPhoto: any;
  stars: number[];
  isLoaded = false;
  bookingForm: FormGroup;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]{1,}\.[a-z]{2,4}$';
  bookingRoomInfo: any;
  roomsArray: any;
  location: any;
  mask = ['+', /\d/, /\d/, ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  displayBookingInfo = false;
  postWrongBookingInfo: any[];
  nights = 0;
  userInfo = new UserInfo();
  liqpayInfo: LiqpayInfo;
  HBNutrition: any;
  policyConfirm = new Array(3).fill(false);
  languagesub: any;
  links: Links;
  params: any;
  bookingproess = false;
  constructor(public activeroute: ActivatedRoute, public bookingservice: BookingHbServices,
              private hotelservice: HotelServices, public globals: Globals,
              public SS: SubscribersServices, private appservices: AppServices) {
    this.HBNutrition = {
      AB: 'hotelpagetable_AB',
      AI: 'hotelpagetable_AI',
      AS: 'hotelpagetable_AS',
      BB: 'hotelpagetable_BB',
      BH: 'hotelpagetable_BH',
      CB: 'hotelpagetable_CB',
      BF: 'hotelpagetable_BF',
      DD: 'hotelpagetable_DD',
      DX: 'hotelpagetable_DX',
      FB: 'hotelpagetable_FB',
      FH: 'hotelpagetable_FH',
      FL: 'hotelpagetable_FL',
      FR: 'hotelpagetable_FR',
      FS: 'hotelpagetable_FS',
      EB: 'hotelpagetable_EB',
      HB: 'hotelpagetable_HB',
      HL: 'hotelpagetable_HL',
      HR: 'hotelpagetable_HR',
      HS: 'hotelpagetable_HS',
      IB: 'hotelpagetable_IB',
      LB: 'hotelpagetable_LB',
      HV: 'hotelpagetable_HV',
      FV: 'hotelpagetable_FV',
      QS: 'hotelpagetable_QS',
      RO: 'hotelpagetable_RO',
      SB: 'hotelpagetable_SB',
      SC: 'hotelpagetable_SC'
    };
    scrollTo(0, 0);
    this.links = new Links('breadcrumps_booking');
    this.hotelid = this.activeroute.snapshot.paramMap.get('id');
    this.liqpayInfo = {};
    this.bookinginfo = this.SS.isLocalSearchData.value;
    this.params = this.appservices.MSDFU(this.bookinginfo);



    const country = this.activeroute.snapshot.paramMap.get('country');
    const city = this.activeroute.snapshot.paramMap.get('city');
    if (country) {
      this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels', country], country));
      if (city) {
        this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels', country, city], city));
      }
    } else {
      this.links.link.push(new Link(['/' + SS.isLanguageCurrent.value, 'hotels'],
        this.appservices.getDirectionBreadcrump(this.bookinginfo)[0]));
    }
    this.links.link.push(new Link(['../'], 'breadcrumps_hotel', this.hotelid));
    this.nights = (+this.bookinginfo.checkOut.setHours(0, 0, 0, 0) - +this.bookinginfo.checkIn.setHours(0, 0, 0, 0)) / 86400000;
    this.bookingRoomInfo = JSON.parse(localStorage.getItem('bookingInfo'));
    console.log(this.bookingRoomInfo);
    if (this.bookingRoomInfo) {
      this.roomsArray = this.bookingservice.roomsCreateArray_hb(this.bookingRoomInfo);
      this.roomsArray.forEach((item, index) => {
        this.bookingservice.getBookingRoomTariffComment_hb(item.id).subscribe(restar => {
          this.roomsArray[index]['tariffdescription'] = restar[0].description;
        });
      });
      console.log(this.roomsArray);
    }
  }
  ngOnInit() {
    this.bookingForm = new FormGroup({
      'work': new FormControl(''),
      'gender': new FormControl(this.userInfo.gender),
      'firstname': new FormControl(this.userInfo.name, [Validators.required]),
      'email': new FormControl(this.userInfo.email, [Validators.pattern(this.emailPattern), Validators.required]),
      'emailconfirm': new FormControl(this.userInfo.email, [Validators.pattern(this.emailPattern), Validators.required]),
      'surname': new FormControl(this.userInfo.surname, [Validators.required]),
      'country': new FormControl(this.userInfo.country),
      'phone': new FormControl(this.userInfo.phone, [Validators.minLength(18)]),
      'description': new FormControl('')
    }, {validators: this.matchingEmails('email', 'emailconfirm')});
    this.bookingservice.getUserInfo().subscribe(resa => {
      this.userInfo.init(resa);
      this.bookingForm.patchValue({firstname: this.userInfo.name, surname: this.userInfo.surname, email: this.userInfo.email,
        emailconfirm: this.userInfo.email, phone: this.userInfo.phone, gender: this.userInfo.gender, country: this.userInfo.country});
    });
    this.languagesub = this.SS.isLanguageCurrent.subscribe(() => {
      this.hotelservice.getHotelInfo(this.hotelid).subscribe(result => {
        this.links.link[this.links.link.length - 1].title = result.title;
        this.hotelInformation = result;
        this.stars = this.hotelInformation.stars ? Array(this.hotelInformation.stars).fill(1) : [];
        if (this.hotelInformation.photos && this.hotelInformation.photos.length > 0) {
          this.avatarPhoto = 'http://photos.hotelbeds.com/giata/bigger/' + this.hotelInformation.photos[0].path;
        } else {
          this.avatarPhoto = '../../../assets/images/hotel-thumb-default.jpg';
        }
        this.isLoaded = true;
      });
    });
  }
  ngOnDestroy() {
    this.languagesub.unsubscribe();
  }
  trackByIdx(index: number, obj: any): any {
    return index;
  }
  checkChildAge(roomindex: number, ageindex: number) {
    if (this.roomsArray[roomindex].childAges[ageindex] < 0) {
      this.roomsArray[roomindex].childAges[ageindex] = 0;
    }
    if (this.roomsArray[roomindex].childAges[ageindex] > 17) {
      this.roomsArray[roomindex].childAges[ageindex] = 17;
    }
  }
  matchingEmails(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {
      [key: string]: any
    } => {
      const pass = group.controls[passwordKey];
      const confirm = group.controls[confirmPasswordKey];

      if (pass.value !== confirm.value) {
        return {
          mismatchedEmails: true
        };
      }
    };
  }
  lowcase() {
    this.bookingForm.patchValue({email: this.bookingForm.value.email.toLowerCase(),
      emailconfirm: this.bookingForm.value.emailconfirm.toLowerCase()});
  }
  formBookData() {
    const bookdata: any = {};
    bookdata['period'] = {
      'from': this.bookinginfo.checkIn,
      'to': this.bookinginfo.checkOut
    };
    bookdata['description'] = this.bookingForm.value.description;
    bookdata['additional'] = {
      'country': this.bookingForm.value.country,
      'goal': this.bookingForm.value.work,
      'gender': this.bookingForm.value.gender
    };
    bookdata['user'] = {
      'name': this.bookingForm.value.firstname,
      'surname': this.bookingForm.value.surname,
      'email': this.bookingForm.value.email,
      'phone': this.bookingForm.value.phone,
      'comment': this.bookingForm.value.description
    };
    bookdata['tariffs'] = [];
    this.roomsArray.forEach((_item, i) => {
      bookdata['tariffs'].push({
        'id': _item.id,
        'name': _item.person.name,
        'surname': _item.person.surname,
        'guestCount': Number(_item.guests.length),
        'childrenAges': _item.childAges,
        'nutrition': _item.nutrition
      });
    });
    return bookdata;
  }
  get firstname() { return this.bookingForm.get('firstname'); }
  get surname() { return this.bookingForm.get('surname'); }
  get email() { return this.bookingForm.get('email'); }
  get emailconfirm() { return this.bookingForm.get('emailconfirm'); }
  get gender() { return this.bookingForm.get('gender'); }
  get country() { return this.bookingForm.get('country'); }
  get description() { return this.bookingForm.get('description'); }
  get phone() { return this.bookingForm.get('phone'); }

  checkChar(event: any) {
    const k = event.charCode;
    if (!(
        (k === 8) || (k === 32) || (k === 45) ||
        (k > 64 && k < 91) || (k > 96 && k < 123) ||
        (k > 1071 && k < 1104) || (k > 1039 && k < 1072)
      )) {
      event.preventDefault();
    }
  }
  confirmingBooking() {
    if (!this.bookingForm.hasError('mismatchedEmails') &&
      this.surname.valid && this.firstname.valid && this.email.valid && this.emailconfirm.valid) {
      this.roomsArray.forEach((item, i) => {
        console.log(item);
        if (this.roomsArray[i].person.name === '') {
          this.roomsArray[i].person.name = this.firstname.value;
        }
        if (this.roomsArray[i].person.surname === '') {
          this.roomsArray[i].person.surname = this.surname.value;
        }
      });
      this.confirmBooking = true;
      scrollTo(0, 0);
    } else {
      this.bookingForm.get('firstname').markAsTouched({onlySelf: true});
      this.bookingForm.get('surname').markAsTouched({onlySelf: true});
      this.bookingForm.get('email').markAsTouched({onlySelf: true});
      this.bookingForm.get('emailconfirm').markAsTouched({onlySelf: true});
    }
  }
  postBooking() {
    if (this.bookingForm.invalid && (!this.confirmBooking || this.phone.invalid)) {
      this.bookingForm.get('phone').markAsTouched({onlySelf: true});
    } else {
      this.bookingproess = true;
      this.bookingservice.getLiqPayInfo(this.formBookData()).subscribe(res => {
        this.bookingproess = false;
        this.displayBookingInfo = true;
        this.postWrongBookingInfo = undefined;
        this.liqpayInfo = res;
        window.location.href = 'https://www.liqpay.ua/api/3/checkout?data=' +
          this.liqpayInfo.data + '&signature=' + this.liqpayInfo.signature;
        console.log('https://www.liqpay.ua/api/3/checkout?data=' + this.liqpayInfo.data + '&signature=' + this.liqpayInfo.signature);
      }, err => {
        this.bookingproess = false;
        this.displayBookingInfo = false;
        this.postWrongBookingInfo = [];
        if (err.status === 409) {
          this.postWrongBookingInfo = err.error;
          console.log(this.postWrongBookingInfo);
        }
      });
    }
  }
}
