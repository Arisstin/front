import {Component, OnDestroy, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookingtableServices} from './bookingtable.services';
import {SubscribersServices} from '../../core/subscribers.services';

@Component({
  selector: 'app-booking-table',
  templateUrl: './bookingtable.component.html',
  styleUrls: ['./bookingtable.component.scss']
})
export class BookingtableComponent implements OnChanges {
  @Input() bookInfo: number;
  @Input() data: any;
  @Input() currency: string;
  @Input() hb: boolean;
  @Output() closeInfo = new EventEmitter<boolean>();
  @Output() backInfo = new EventEmitter<boolean>();
  info: any;
  bookid: string;
  bb: string;

  constructor(private bookingtableservices: BookingtableServices, private activeroute: ActivatedRoute, public SS: SubscribersServices) {
    this.bookid = this.activeroute.snapshot.paramMap.get('id');
    this.bb = this.activeroute.snapshot.paramMap.get('bb');
    if (this.bookid) {
      if (this.bb) {
        this.bookingtableservices.getBookInfo_bb(Number(this.bookid)).subscribe(res => {
          this.info = res;
        });
      } else {
        this.bookingtableservices.getBookInfo(Number(this.bookid)).subscribe(res => {
          this.info = res;
          this.countFullPrice();
        });
      }
    }
  }
  ngOnChanges() {
    if (this.bookInfo) {
      if (this.hb) {
        this.bookingtableservices.getBookInfo_bb(this.bookInfo).subscribe(res => {
          window.scrollTo(0, 0);
          this.info = res;
        });
      } else {
        this.bookingtableservices.getBookInfo(this.bookInfo).subscribe(res => {
          window.scrollTo(0, 0);
          this.info = res;
          this.countFullPrice();
        });
      }
    }
    if (this.data) {
      this.info = this.data;
      window.scrollTo(0, 0);
      this.countFullPrice();
    }
  }
  back() {
    this.backInfo.emit(true);
  }
  close() {
    this.closeInfo.emit(false);
  }
  hb_mans(data: any[]) {
    let result = '';
    let adultcount = 0;
    let childrenstring = '';
    data.forEach(item => {
      if (item.type === 'AD') {
        adultcount++;
      } else {
        if (childrenstring === '') { childrenstring += ' and '; }
        childrenstring += '1 child ' + item.age + ' year(s) old, ';
      }
    });
    result += adultcount + ' adult(s)' + childrenstring;
    return result;
  }
  print(): void {
    let printContents, popupWin;
    const stylesHtml = this.getTagsHtml('style');
    const linksHtml = this.getTagsHtml('link');
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
           ${linksHtml}
            ${stylesHtml}
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
  private getTagsHtml(tagName: keyof HTMLElementTagNameMap): string {
    const htmlStr: string[] = [];
    const elements = document.getElementsByTagName(tagName);
    for (let idx = 0; idx < elements.length; idx++) {
      htmlStr.push(elements[idx].outerHTML);
    }
    return htmlStr.join('\r\n');
  }
  countFullPrice() {
    this.info['fullprice'] = 0;
    this.info.rooms.forEach((item, i) => {
      let fullroomprice = 0;
      item.days.forEach(it => {
        fullroomprice += it.price;
      });
      this.info.rooms[i]['fullroomprice'] = fullroomprice;
      this.info.fullprice += fullroomprice;
      if (item.nutritions[0]) {
        this.info.fullprice += item.nutritions[0].price;
      }
    });
  }
}
