import { Injectable } from '@angular/core';
import { BaseService } from '../../core/base.services';
import { Observable} from 'rxjs';

@Injectable()
export class BookingtableServices extends BaseService {
  getBookInfo(id: number): Observable<any> {
    return this.get('bookings/' + id);
  }
  getBookInfo_bb(id: number): Observable<any> {
    return this.get('booking/' + id + '/bb');
  }
}
